<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Adminhtml_Cms_NavigationController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @param bool $getRootInstead
     *
     * @return bool
     */
    protected function _initElement($getRootInstead = false)
    {
        $this->_title($this->__('CMS'))
            ->_title($this->__('Navigation'))
            ->_title($this->__('Manage Elements'));

        $elementId = (int)$this->getRequest()->getParam('id', false);
        $storeId   = (int)$this->getRequest()->getParam('store');
        $element   = Mage::getModel('poebel_cmsnavigation/element');
        $element->setStoreId($storeId);

        if ($elementId) {
            $element->load($elementId);
            if ($storeId) {
                $rootId = Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID, $storeId);
                if (!in_array($rootId, $element->getPathIds())) {
                    // load root category instead wrong one
                    if ($getRootInstead) {
                        $element->load($rootId);
                    } else {
                        $this->_redirect('*/*/', array('_current' => true, 'id' => null));
                        return false;
                    }
                }
            }
        }

        if ($activeTabId = (string)$this->getRequest()->getParam('active_tab_id')) {
            Mage::getSingleton('admin/session')->setActiveTabId($activeTabId);
        }

        Mage::register('element', $element);
        Mage::register('current_element', $element);
        return $element;
    }

    /**
     *
     */
    public function indexAction()
    {
        $this->_forward('edit');
    }

    /**
     *
     */
    public function addAction()
    {
        Mage::getSingleton('admin/session')->unsElementActiveTabId();
        $this->_forward('edit');
    }

    /**
     *
     */
    public function editAction()
    {
        $params['_current'] = true;
        $redirect           = false;

        $storeId      = (int)$this->getRequest()->getParam('store');
        $parentId     = (int)$this->getRequest()->getParam('parent');
        $_prevStoreId = Mage::getSingleton('admin/session')->getElementLastViewedStore(true);

        if (!empty($_prevStoreId) && !$this->getRequest()->getQuery('isAjax')) {
            $params['store'] = $_prevStoreId;
            $redirect        = true;
        }

        $elementId      = (int)$this->getRequest()->getParam('id');
        $_prevElementId = Mage::getSingleton('admin/session')->getLastEditedElement(true);

        if ($_prevElementId
            && !$this->getRequest()->getQuery('isAjax')
            && !$this->getRequest()->getParam('clear')
        ) {
            $this->getRequest()->setParam('id', $_prevElementId);
        }

        if ($redirect) {
            $this->_redirect('*/*/edit', $params);
            return;
        }

        if ($storeId && !$elementId && !$parentId) {
            $store          = Mage::app()->getStore($storeId);
            $_prevElementId = 0;
            if ($store->getGroup()) {
                $_prevElementId = Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID, $store->getId());
            }
            $this->getRequest()->setParam('id', $_prevElementId);
        }

        if (!($element = $this->_initElement(true))) {
            return;
        }

        $this->_title($elementId ? $element->getName() : $this->__('New Navigation Element'));

        $data = Mage::getSingleton('adminhtml/session')->getElementData(true);
        if (isset($data['general'])) {
            $element->addData($data['general']);
        }

        if ($this->getRequest()->getQuery('isAjax')) {
            // prepare breadcrumbs of selected element, if any
            $breadcrumbsPath = $element->getPath();
            if (empty($breadcrumbsPath)) {
                // but if no element, and it is deleted - prepare breadcrumbs from path, saved in session
                $breadcrumbsPath = Mage::getSingleton('admin/session')->getElementDeletedPath(true);
                if (!empty($breadcrumbsPath)) {
                    $breadcrumbsPath = explode('/', $breadcrumbsPath);
                    // no need to get parent breadcrumbs if deleting category level 1
                    if (count($breadcrumbsPath) <= 1) {
                        $breadcrumbsPath = '';
                    } else {
                        array_pop($breadcrumbsPath);
                        $breadcrumbsPath = implode('/', $breadcrumbsPath);
                    }
                }
            }

            Mage::getSingleton('admin/session')
                ->setElementLastViewedStore($this->getRequest()->getParam('store'));
            Mage::getSingleton('admin/session')
                ->setLastEditedElement($element->getId());
            $this->loadLayout();

            $eventResponse = new Varien_Object(array(
                'content'  => $this->getLayout()->getBlock('element.edit')->getFormHtml()
                . $this->getLayout()->getBlock('element.tree')
                    ->getBreadcrumbsJavascript($breadcrumbsPath, 'editingElementBreadcrumbs'),
                'messages' => $this->getLayout()->getMessagesBlock()->getGroupedHtml(),
            ));

            Mage::dispatchEvent(
                'poebel_cmsnavigation_element_prepare_ajax_response', array(
                    'response'   => $eventResponse,
                    'controller' => $this
                )
            );

            $this->getResponse()->setBody(
                Mage::helper('core')->jsonEncode($eventResponse->getData())
            );

            return;
        }

        $this->loadLayout();
        $this->_setActiveMenu('cms/navigation');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true)
            ->setContainerCssClass('catalog-categories');

        $this->_addBreadcrumb(
            Mage::helper('poebel_cmsnavigation')->__('Manage CMS Navigation Elements'),
            Mage::helper('poebel_cmsnavigation')->__('Manage CMS Navigation')
        );

        $this->renderLayout();
    }

    /**
     *
     */
    public function elementsJsonAction()
    {
        if ($this->getRequest()->getParam('expand_all')) {
            Mage::getSingleton('admin/session')->setElementIsTreeWasExpanded(true);
        } else {
            Mage::getSingleton('admin/session')->setElementIsTreeWasExpanded(false);
        }
        if ($elementId = (int)$this->getRequest()->getPost('id')) {
            $this->getRequest()->setParam('id', $elementId);

            if (!$element = $this->_initElement()) {
                return;
            }
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock('poebel_cmsnavigation/adminhtml_element_tree')
                    ->getTreeJson($element)
            );
        }
    }

    /**
     *
     */
    public function saveAction()
    {
        if (!$element = $this->_initElement()) {
            return;
        }

        $storeId     = $this->getRequest()->getParam('store');
        $refreshTree = 'false';
        if ($data = $this->getRequest()->getPost()) {
            $element->addData($data['general']);
            if (!$element->getId()) {
                $parentId = $this->getRequest()->getParam('parent');
                if (!$parentId) {
                    if ($storeId) {
                        $parentId = Mage::app()->getStore($storeId)->getRootCategoryId();
                    } else {
                        $parentId = Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID;
                    }
                }
                $parentElement = Mage::getModel('poebel_cmsnavigation/element')->load($parentId);
                $element->setPath($parentElement->getPath());
            }

            if ($useDefaults = $this->getRequest()->getPost('use_default')) {
                foreach ($useDefaults as $attributeCode) {
                    $element->setData($attributeCode, false);
                }
            }

            if ($useConfig = $this->getRequest()->getPost('use_config')) {
                foreach ($useConfig as $attributeCode) {
                    $element->setData($attributeCode, null);
                }
            }

            $element->setAttributeSetId($element->getDefaultAttributeSetId());

            Mage::dispatchEvent(
                'poebel_cmsnavigatione_element_prepare_save', array(
                    'category' => $element,
                    'request'  => $this->getRequest()
                )
            );

            $element->setData('use_post_data_config', $this->getRequest()->getPost('use_config'));

            try {
                $validate = $element->validate();
                if ($validate !== true) {
                    foreach ($validate as $code => $error) {
                        if ($error === true) {
                            Mage::throwException(
                                Mage::helper('poebel_cmsnavigation')->__('Attribute "%s" is required.', $element->getResource()->getAttribute($code)->getFrontend()->getLabel())
                            );
                        } else {
                            Mage::throwException($error);
                        }
                    }
                }

                $element->unsetData('use_post_data_config');

                $element->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('poebel_cmsnavigation')->__('The element has been saved.'));
                $refreshTree = 'true';
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage())
                    ->setElementData($data);
                $refreshTree = 'false';
            }
        }
        $url = $this->getUrl('*/*/edit', array('_current' => true, 'id' => $element->getId()));
        $this->getResponse()->setBody(
            '<script type="text/javascript">parent.updateContent("' . $url . '", {}, ' . $refreshTree . ');</script>'
        );
    }

    /**
     *
     */
    public function moveAction()
    {
        $element = $this->_initElement();
        if (!$element) {
            $this->getResponse()->setBody(Mage::helper('poebel_cmsnavigation')->__('Element move error'));
            return;
        }

        $parentNodeId = $this->getRequest()->getPost('pid', false);

        $prevNodeId = $this->getRequest()->getPost('aid', false);
        try {
            $element->move($parentNodeId, $prevNodeId);
            $this->getResponse()->setBody("SUCCESS");
        } catch (Mage_Core_Exception $e) {
            $this->getResponse()->setBody($e->getMessage());
        }
        catch (Exception $e) {
            $this->getResponse()->setBody(Mage::helper('poebel_cmsnavigation')->__('Element move error %s', $e));
            Mage::logException($e);
        }
    }

    /**
     *
     */
    public function deleteAction()
    {
        if ($id = (int)$this->getRequest()->getParam('id')) {
            try {
                $element = Mage::getModel('poebel_cmsnavigation/element')->load($id);
                Mage::dispatchEvent('poebel_cmsnavigation_controller_element_delete', array('element' => $element));

                Mage::getSingleton('admin/session')->setElementDeletedPath($element->getPath());

                $element->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('poebel_cmsnavigation')->__('The element has been deleted.'));
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->getResponse()->setRedirect($this->getUrl('*/*/edit', array('_current' => true)));
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('poebel_cmsnavigation')->__('An error occurred while trying to delete the element.'));
                $this->getResponse()->setRedirect($this->getUrl('*/*/edit', array('_current' => true)));
                return;
            }
        }
        $this->getResponse()->setRedirect($this->getUrl('*/*/', array('_current' => true, 'id' => null)));
    }

    /**
     *
     */
    public function treeAction()
    {
        $storeId   = (int)$this->getRequest()->getParam('store');
        $elementId = (int)$this->getRequest()->getParam('id');

        if ($storeId) {
            if (!$elementId) {
                $store  = Mage::app()->getStore($storeId);
                $rootId = 0;
                if ($store->getGroup()) {
                    $rootId = Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID, $store->getId());
                }
                $this->getRequest()->setParam('id', $rootId);
            }
        }

        $element = $this->_initElement(true);

        $block = $this->getLayout()->createBlock('poebel_cmsnavigation/adminhtml_element_tree');
        $root  = $block->getRoot();
        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode(
                array(
                    'data'       => $block->getTree(),
                    'parameters' => array(
                        'text'         => $block->buildNodeName($root),
                        'draggable'    => false,
                        'allowDrop'    => ($root->getIsVisible()) ? true : false,
                        'id'           => (int)$root->getId(),
                        'expanded'     => (int)$block->getElementIsWasExpanded(),
                        'store_id'     => (int)$block->getStore()->getId(),
                        'element_id'   => (int)$element->getId(),
                        'root_visible' => (int)$root->getIsVisible()
                    )
                )
            )
        );
    }

    /**
     *
     */
    public function refreshPathAction()
    {
        if ($id = (int)$this->getRequest()->getParam('id')) {
            $element = Mage::getModel('poebel_cmsnavigation/element')->load($id);
            $this->getResponse()->setBody(
                Mage::helper('core')->jsonEncode(
                    array(
                        'id'   => $id,
                        'path' => $element->getPath(),
                    )
                )
            );
        }
    }

    /**
     *
     */
    public function chooserAction()
    {
        try {
            $chooser = $this->getLayout()->createBlock(
                'poebel_cmsnavigation/adminhtml_chooser_' . $this->getRequest()->getParam('type'),
                '',
                array(
                    'id'           => Mage::helper('core')->uniqHash($this->getRequest()->getParam('uniq_id')),
                    'element_id'   => $this->getRequest()->getParam('uniq_id'),
                    'chooser_type' => $this->getRequest()->getParam('type'),
                )
            );
            if ($chooser instanceof Mage_Adminhtml_Block_Widget_Grid) {
                $this->getResponse()->setBody($chooser->toHtml());
            } else {
                $this->getResponse()->setBody($this->__('Failed to load chooser'));
            }
        } catch (Exception $e) {
            $this->getResponse()->setBody($e->getMessage());
        }
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/navigation');
    }
}
