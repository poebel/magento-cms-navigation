<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

/* @var $installer Poebel_CmsNavigation_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'poebel_cmsnavigation/category'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('poebel_cmsnavigation/element'))
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'Entity ID'
    )
    ->addColumn(
        'entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity Type ID'
    )
    ->addColumn(
        'attribute_set_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Attribute Set ID'
    )
    ->addColumn(
        'parent_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Parent Element ID'
    )
    ->addColumn(
        'created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time'
    )
    ->addColumn(
        'updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Update Time'
    )
    ->addColumn(
        'path', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
        ), 'Tree Path'
    )
    ->addColumn(
        'position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'Position'
    )
    ->addColumn(
        'level', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'default'  => '0',
        ), 'Tree Level'
    )
    ->addColumn(
        'children_count', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'Child Count'
    )
    ->addIndex(
        $installer->getIdxName('poebel_cmsnavigation/element', array('level')),
        array('level')
    )
    ->addIndex(
        $installer->getIdxName('poebel_cmsnavigation/element', array('path', 'entity_id')),
        array('path', 'entity_id')
    )
    ->setComment('CMS Navigation Element Table');
$installer->getConnection()->createTable($table);

/**
 * Create table array('poebel_cmsnavigation/element', 'datetime')
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable(array('poebel_cmsnavigation/element', 'datetime')))
    ->addColumn(
        'value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'Value ID'
    )
    ->addColumn(
        'entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity Type ID'
    )
    ->addColumn(
        'attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Attribute ID'
    )
    ->addColumn(
        'store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Store ID'
    )
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity ID'
    )
    ->addColumn(
        'value', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(), 'Value'
    )
    ->addIndex(
        $installer->getIdxName(
            array('poebel_cmsnavigation/element', 'datetime'),
            array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'datetime'), array('entity_id')),
        array('entity_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'datetime'), array('attribute_id')),
        array('attribute_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'datetime'), array('store_id')),
        array('store_id')
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'datetime'), 'attribute_id', 'eav/attribute', 'attribute_id'),
        'attribute_id', $installer->getTable('eav/attribute'), 'attribute_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'datetime'), 'entity_id', 'poebel_cmsnavigation/element', 'entity_id'),
        'entity_id', $installer->getTable('poebel_cmsnavigation/element'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'datetime'), 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('CMS Navigation Element Datetime Attribute Backend Table');
$installer->getConnection()->createTable($table);

/**
 * Create table array('poebel_cmsnavigation/element', 'decimal')
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable(array('poebel_cmsnavigation/element', 'decimal')))
    ->addColumn(
        'value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'Value ID'
    )
    ->addColumn(
        'entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity Type ID'
    )
    ->addColumn(
        'attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Attribute ID'
    )
    ->addColumn(
        'store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Store ID'
    )
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity ID'
    )
    ->addColumn(
        'value', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(), 'Value'
    )
    ->addIndex(
        $installer->getIdxName(
            array('poebel_cmsnavigation/element', 'decimal'),
            array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'decimal'), array('entity_id')),
        array('entity_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'decimal'), array('attribute_id')),
        array('attribute_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'decimal'), array('store_id')),
        array('store_id')
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'decimal'), 'attribute_id', 'eav/attribute', 'attribute_id'),
        'attribute_id', $installer->getTable('eav/attribute'), 'attribute_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'decimal'), 'entity_id', 'poebel_cmsnavigation/element', 'entity_id'),
        'entity_id', $installer->getTable('poebel_cmsnavigation/element'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'decimal'), 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('CMS Navigation Element Decimal Attribute Backend Table');
$installer->getConnection()->createTable($table);

/**
 * Create table array('poebel_cmsnavigation/element', 'int')
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable(array('poebel_cmsnavigation/element', 'int')))
    ->addColumn(
        'value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'Value ID'
    )
    ->addColumn(
        'entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity Type ID'
    )
    ->addColumn(
        'attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Attribute ID'
    )
    ->addColumn(
        'store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Store ID'
    )
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity ID'
    )
    ->addColumn(
        'value', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(), 'Value'
    )
    ->addIndex(
        $installer->getIdxName(
            array('poebel_cmsnavigation/element', 'int'),
            array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'int'), array('entity_id')),
        array('entity_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'int'), array('attribute_id')),
        array('attribute_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'int'), array('store_id')),
        array('store_id')
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'int'), 'attribute_id', 'eav/attribute', 'attribute_id'),
        'attribute_id', $installer->getTable('eav/attribute'), 'attribute_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'int'), 'entity_id', 'poebel_cmsnavigation/element', 'entity_id'),
        'entity_id', $installer->getTable('poebel_cmsnavigation/element'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'int'), 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('CMS Navigation Element Integer Attribute Backend Table');
$installer->getConnection()->createTable($table);

/**
 * Create table array('poebel_cmsnavigation/element', 'text')
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable(array('poebel_cmsnavigation/element', 'text')))
    ->addColumn(
        'value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'Value ID'
    )
    ->addColumn(
        'entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity Type ID'
    )
    ->addColumn(
        'attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Attribute ID'
    )
    ->addColumn(
        'store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Store ID'
    )
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity ID'
    )
    ->addColumn(
        'value', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Value'
    )
    ->addIndex(
        $installer->getIdxName(
            array('poebel_cmsnavigation/element', 'text'),
            array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'text'), array('entity_id')),
        array('entity_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'text'), array('attribute_id')),
        array('attribute_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'text'), array('store_id')),
        array('store_id')
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'text'), 'attribute_id', 'eav/attribute', 'attribute_id'),
        'attribute_id', $installer->getTable('eav/attribute'), 'attribute_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'text'), 'entity_id', 'poebel_cmsnavigation/element', 'entity_id'),
        'entity_id', $installer->getTable('poebel_cmsnavigation/element'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'text'), 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('CMS Navigation Element Text Attribute Backend Table');
$installer->getConnection()->createTable($table);

/**
 * Create table array('poebel_cmsnavigation/element', 'varchar')
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable(array('poebel_cmsnavigation/element', 'varchar')))
    ->addColumn(
        'value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'Value ID'
    )
    ->addColumn(
        'entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity Type ID'
    )
    ->addColumn(
        'attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Attribute ID'
    )
    ->addColumn(
        'store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Store ID'
    )
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'default'  => '0',
        ), 'Entity ID'
    )
    ->addColumn(
        'value', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Value'
    )
    ->addIndex(
        $installer->getIdxName(
            array('poebel_cmsnavigation/element', 'varchar'),
            array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('entity_type_id', 'entity_id', 'attribute_id', 'store_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'varchar'), array('entity_id')),
        array('entity_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'varchar'), array('attribute_id')),
        array('attribute_id')
    )
    ->addIndex(
        $installer->getIdxName(array('poebel_cmsnavigation/element', 'varchar'), array('store_id')),
        array('store_id')
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'varchar'), 'attribute_id', 'eav/attribute', 'attribute_id'),
        'attribute_id', $installer->getTable('eav/attribute'), 'attribute_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'varchar'), 'entity_id', 'poebel_cmsnavigation/element', 'entity_id'),
        'entity_id', $installer->getTable('poebel_cmsnavigation/element'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(array('poebel_cmsnavigation/element', 'varchar'), 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('CMS Navigation Element Varchar Attribute Backend Table');
$installer->getConnection()->createTable($table);


/**
 * Create table 'poebel_cmsnavigation/eav_attribute'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('poebel_cmsnavigation/eav_attribute'))
    ->addColumn('attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Attribute ID')
    ->addColumn('frontend_input_renderer', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Frontend Input Renderer')
    ->addColumn('is_global', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '1',
        ), 'Is Global')
    ->addColumn('is_visible', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '1',
        ), 'Is Visible')
    ->addColumn('is_visible_on_front', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '0',
        ), 'Is Visible On Front')
    ->addColumn('is_html_allowed_on_front', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '0',
        ), 'Is HTML Allowed On Front')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => false,
            'default'   => '0',
        ), 'Position')
    ->addColumn('is_wysiwyg_enabled', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '0',
        ), 'Is WYSIWYG Enabled')
    ->addForeignKey($installer->getFkName('poebel_cmsnavigation/eav_attribute', 'attribute_id', 'eav/attribute', 'attribute_id'),
        'attribute_id', $installer->getTable('eav/attribute'), 'attribute_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('CMS Navigation EAV Attribute Table');
$installer->getConnection()->createTable($table);

$installer->endSetup();

$installer->installEntities();