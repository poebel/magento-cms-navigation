<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

/* @var $installer Poebel_CmsNavigation_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute(
    Poebel_CmsNavigation_Model_Element::ENTITY, 'cms_page_id', array(
        'frontend_input'          => 'text',
        'frontend_input_renderer' => 'poebel_cmsnavigation/adminhtml_form_element_chooser_page',
        'source_model'            => null,
    )
);

$installer->endSetup();