<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

/** @var Poebel_CmsNavigation_Model_Resource_Setup $installer */
$installer = $this;

// Create Root CMS Navigation Node
Mage::getModel('poebel_cmsnavigation/element')
    ->load(Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID)
    ->setId(1)
    ->setStoreId(0)
    ->setPath(1)
    ->setLevel(0)
    ->setPosition(0)
    ->setChildrenCount(0)
    ->setName('Root Element')
    ->setInitialSetupFlag(true)
    ->save();

/** @var Poebel_CmsNavigation_Model_Element $element */
$element = Mage::getModel('poebel_cmsnavigation/element');

$element->setStoreId(0)
    ->setName('Default Element')
    ->setIsActive(1)
    ->setPath('1')
    ->setInitialSetupFlag(true)
    ->save();

$installer->setConfigData(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID, $element->getId());
