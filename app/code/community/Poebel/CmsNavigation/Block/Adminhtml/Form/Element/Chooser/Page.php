<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Form_Element_Chooser_Page extends Poebel_CmsNavigation_Block_Adminhtml_Form_Element_Chooser_Abstract
{
    /**
     *
     */
    protected function _init()
    {
        if ($this->getValue()) {
            $page = Mage::getModel('cms/page')->load($this->getValue());
            if ($page->getId()) {
                $pageName = $page->getTitle() . ' (' . $page->getIdentifier() . ')';
            } else {
                $pageName = Mage::helper('poebel_cmsnavigation')->__('Deleted Page');
            }
            $this->setSelectLabel(Mage::helper('poebel_cmsnavigation')->__('Selected page:') . ' ' . $pageName);
        } else {
            $this->setSelectLabel(Mage::helper('poebel_cmsnavigation')->__('No page selected'));
        }
        $this->setSelectButtonLabel(Mage::helper('poebel_cmsnavigation')->__('Select Page'));
        $this->setSelectFunctionType('cms_page');
    }
}