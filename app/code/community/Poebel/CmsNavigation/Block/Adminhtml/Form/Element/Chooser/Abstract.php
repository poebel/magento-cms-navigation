<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

abstract class Poebel_CmsNavigation_Block_Adminhtml_Form_Element_Chooser_Abstract extends Varien_Data_Form_Element_Text
{
    /**
     * @return mixed
     */
    abstract protected function _init();

    /**
     * @return string
     */
    public function getElementHtml()
    {
        $this->_init();

        $html = '<input id="' . $this->getHtmlId() . '" name="'
            . $this->getName() . '" value="' . $this->getEscapedValue() . '" type="hidden"/>' . "\n";

        $html .= '<span id="' . $this->getHtmlId() . '_label" style="display: inline-block; margin-bottom: 4px;">' . $this->getSelectLabel() . '</span><br/>' . "\n";

        $html .= '<button onclick="openChooser(\'' . $this->getHtmlId() . '\', \'' . $this->getSelectFunctionType() . '\', \''
            . $this->getSelectButtonLabel() . '\')" class="scalable' . ($this->getDisabled() ? ' disabled"  disabled' : '') . '" type="button" title="'
            . $this->getSelectButtonLabel() . '"><span><span><span>' . $this->getSelectButtonLabel() . '</span></span></span></button>' . "\n";

        $html .= $this->getAfterElementHtml();

        return $html;
    }
}