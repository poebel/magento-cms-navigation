<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Form_Element_Mode extends Varien_Data_Form_Element_Select
{
    /**
     * @return string
     */
    public function getAfterElementHtml()
    {
        $html
            = '<script type="text/javascript">
        //<![CDATA[
        Event.observe($("' . $this->getHtmlId() . '"), "change", toggleModeElement);

        function toggleModeElement() {
            var allModeElements = new Array("cms_page_id", "action", "url", "category_id", "product_id");
            var showModeElement = "";

            switch (parseInt($F("' . $this->getHtmlId() . '"))) {
                case ' . Poebel_CmsNavigation_Model_Element::MODE_CMS_PAGE . ':
                    showModeElement = "cms_page_id";
                    break;
                case ' . Poebel_CmsNavigation_Model_Element::MODE_ACTION . ':
                    showModeElement = "action";
                    break;
                case ' . Poebel_CmsNavigation_Model_Element::MODE_URL . ':
                    showModeElement = "url";
                    break;
                case ' . Poebel_CmsNavigation_Model_Element::MODE_CATEGORY . ':
                    showModeElement = "category_id";
                    break;
                case ' . Poebel_CmsNavigation_Model_Element::MODE_PRODUCT . ':
                    showModeElement = "product_id";
                    break;
            }

            allModeElements.forEach(function(element) {
                var elementId = "' . $this->getForm()->getHtmlIdPrefix() . '" + element + "' . $this->getForm()->getHtmlIdSuffix() . '";
                if($(elementId)) {
                    if(element == showModeElement) {
                        $(elementId).parentNode.parentNode.show();
                    } else {
                        $(elementId).parentNode.parentNode.hide();
                    }
                }
            });
        }

        toggleModeElement();
        //]]>
        </script>';

        $html .= parent::getAfterElementHtml();

        return $html;
    }
}