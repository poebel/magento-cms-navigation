<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Form_Element_Chooser_Product extends Poebel_CmsNavigation_Block_Adminhtml_Form_Element_Chooser_Abstract
{
    /**
     *
     */
    protected function _init()
    {
        if ($this->getValue()) {
            if (Mage::helper('core')->isModuleEnabled('Mage_Catalog')) {
                $product = Mage::getModel('catalog/product')->load($this->getValue());
                if ($product->getId()) {
                    $productName = $product->getName() . ' (' . $product->getId() . ')';
                } else {
                    $productName = Mage::helper('poebel_cmsnavigation')->__('Deleted Product');
                }
            } else {
                $productName = 'Mage_Catalog disabled';
            }
            $this->setSelectLabel(Mage::helper('poebel_cmsnavigation')->__('Selected product:') . ' ' . $productName);
        } else {
            $this->setSelectLabel(Mage::helper('poebel_cmsnavigation')->__('No product selected'));
        }
        $this->setSelectButtonLabel(Mage::helper('poebel_cmsnavigation')->__('Select Product'));
        $this->setSelectFunctionType('product');
    }
}