<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Form_Element_Chooser_Category extends Poebel_CmsNavigation_Block_Adminhtml_Form_Element_Chooser_Abstract
{
    /**
     *
     */
    protected function _init()
    {
        if ($this->getValue()) {
            if (Mage::helper('core')->isModuleEnabled('Mage_Catalog')) {
                $category = Mage::getModel('catalog/category')->load($this->getValue());
                if ($category->getId()) {
                    $categoryName = $category->getName() . ' (' . $category->getId() . ')';
                } else {
                    $categoryName = Mage::helper('poebel_cmsnavigation')->__('Deleted Category');
                }
            } else {
                $categoryName = 'Mage_Catalog disabled';
            }
            $this->setSelectLabel(Mage::helper('poebel_cmsnavigation')->__('Selected category:') . ' ' . $categoryName);
        } else {
            $this->setSelectLabel(Mage::helper('poebel_cmsnavigation')->__('No category selected'));
        }
        $this->setSelectButtonLabel(Mage::helper('poebel_cmsnavigation')->__('Select Category'));
        $this->setSelectFunctionType('category');
    }
}