<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Element_Tree extends Poebel_CmsNavigation_Block_Adminhtml_Element_Abstract
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('poebel/cmsnavigation/element/tree.phtml');
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $addUrl = $this->getUrl(
            "*/*/add", array(
                '_current' => true,
                'id'       => null,
                '_query'   => false
            )
        );

        $this->setChild(
            'add_sub_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(
                    array(
                        'label'   => Mage::helper('poebel_cmsnavigation')->__('Add Sub Element'),
                        'onclick' => "addNew('" . $addUrl . "', false)",
                        'class'   => 'add',
                        'id'      => 'add_subelement_button',
                        'style'   => $this->canAddSubElement() ? '' : 'display: none;'
                    )
                )
        );

        if ($this->canAddRootElement()) {
            $this->setChild(
                'add_root_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(
                        array(
                            'label'   => Mage::helper('poebel_cmsnavigation')->__('Add Root Element'),
                            'onclick' => "addNew('" . $addUrl . "', true)",
                            'class'   => 'add',
                            'id'      => 'add_root_element_button'
                        )
                    )
            );
        }

        $this->setChild(
            'store_switcher',
            $this->getLayout()->createBlock('adminhtml/store_switcher')
                ->setSwitchUrl($this->getUrl('*/*/*', array('_current' => true, '_query' => false, 'store' => null)))
                ->setTemplate('store/switcher/enhanced.phtml')
        );
        return parent::_prepareLayout();
    }

    /**
     * @return int
     */
    protected function _getDefaultStoreId()
    {
        return Poebel_CmsNavigation_Model_Abstract::DEFAULT_STORE_ID;
    }

    /**
     * @return mixed
     */
    public function getElementCollection()
    {
        $storeId    = $this->getRequest()->getParam('store', $this->_getDefaultStoreId());
        $collection = $this->getData('element_collection');
        if (is_null($collection)) {
            $collection = Mage::getResourceModel('poebel_cmsnavigation/element_collection');

            $collection->addAttributeToSelect('name')
                ->addAttributeToSelect('is_active')
                ->setStoreId($storeId);

            $this->setData('element_collection', $collection);
        }
        return $collection;
    }

    /**
     * @return string
     */
    public function getAddRootButtonHtml()
    {
        return $this->getChildHtml('add_root_button');
    }

    /**
     * @return string
     */
    public function getAddSubButtonHtml()
    {
        return $this->getChildHtml('add_sub_button');
    }

    /**
     * @return string
     */
    public function getExpandButtonHtml()
    {
        return $this->getChildHtml('expand_button');
    }

    /**
     * @return string
     */
    public function getCollapseButtonHtml()
    {
        return $this->getChildHtml('collapse_button');
    }

    /**
     * @return string
     */
    public function getStoreSwitcherHtml()
    {
        return $this->getChildHtml('store_switcher');
    }

    /**
     * @param null $expanded
     *
     * @return string
     */
    public function getLoadTreeUrl($expanded = null)
    {
        $params = array('_current' => true, 'id' => null, 'store' => null);
        if (
            (is_null($expanded) && Mage::getSingleton('admin/session')->getElementIsTreeWasExpanded())
            || $expanded == true
        ) {
            $params['expand_all'] = true;
        }
        return $this->getUrl('*/*/elementsJson', $params);
    }

    /**
     * @return string
     */
    public function getNodesUrl()
    {
        return $this->getUrl('*/cms_navigation/jsonTree');
    }

    /**
     * @return string
     */
    public function getSwitchTreeUrl()
    {
        return $this->getUrl(
            "*/cms_navigation/tree",
            array('_current' => true, 'store' => null, '_query' => false, 'id' => null, 'parent' => null)
        );
    }

    /**
     * @return mixed
     */
    public function getIsWasExpanded()
    {
        return Mage::getSingleton('admin/session')->getElementIsTreeWasExpanded();
    }

    /**
     * @return string
     */
    public function getMoveUrl()
    {
        return $this->getUrl('*/cms_navigation/move', array('store' => $this->getRequest()->getParam('store')));
    }

    /**
     * @param null $parenNodeElement
     *
     * @return array
     */
    public function getTree($parenNodeElement = null)
    {
        $rootArray = $this->_getNodeJson($this->getRoot($parenNodeElement));
        $tree      = isset($rootArray['children']) ? $rootArray['children'] : array();
        return $tree;
    }

    /**
     * @param null $parentNodeElement
     *
     * @return string
     */
    public function getTreeJson($parentNodeElement = null)
    {
        $rootArray = $this->_getNodeJson($this->getRoot($parentNodeElement));
        $json      = Mage::helper('core')->jsonEncode(isset($rootArray['children']) ? $rootArray['children'] : array());
        return $json;
    }

    /**
     * @param $path
     * @param $javascriptVarName
     *
     * @return string
     */
    public function getBreadcrumbsJavascript($path, $javascriptVarName)
    {
        if (empty($path)) {
            return '';
        }

        $elements = Mage::getResourceSingleton('poebel_cmsnavigation/element_tree')
            ->setStoreId($this->getStore()->getId())->loadBreadcrumbsArray($path);
        if (empty($elements)) {
            return '';
        }
        foreach ($elements as $key => $element) {
            $elements[$key] = $this->_getNodeJson($element);
        }
        return
            '<script type="text/javascript">'
            . $javascriptVarName . ' = ' . Mage::helper('core')->jsonEncode($elements) . ';'
            . ($this->canAddSubElement()
                ? '$("add_subelement_button").show();'
                : '$("add_subelement_button").hide();')
            . '</script>';
    }

    /**
     * @param     $node
     * @param int $level
     *
     * @return array
     */
    protected function _getNodeJson($node, $level = 0)
    {
        // create a node from data array
        if (is_array($node)) {
            $node = new Varien_Data_Tree_Node($node, 'entity_id', new Varien_Data_Tree);
        }

        $item         = array();
        $item['text'] = $this->buildNodeName($node);

        $rootForStores = in_array($node->getEntityId(), $this->getRootIds());

        $item['id']    = $node->getId();
        $item['store'] = (int)$this->getStore()->getId();
        $item['path']  = $node->getData('path');

        $item['cls']       = 'folder ' . ($node->getIsActive() ? 'active-category' : 'no-active-category');
        $allowMove         = $this->_isElementMoveable($node);
        $item['allowDrop'] = $allowMove;
        $item['allowDrag'] = $allowMove && (($node->getLevel() == 1 && $rootForStores) ? false : true);

        if ((int)$node->getChildrenCount() > 0) {
            $item['children'] = array();
        }

        $isParent = $this->_isParentSelectedElement($node);

        if ($node->hasChildren()) {
            $item['children'] = array();
            if (!($this->getUseAjax() && $node->getLevel() > 1 && !$isParent)) {
                foreach ($node->getChildren() as $child) {
                    $item['children'][] = $this->_getNodeJson($child, $level + 1);
                }
            }
        }

        if ($isParent || $node->getLevel() < 2) {
            $item['expanded'] = true;
        }

        return $item;
    }

    /**
     * @param $node
     *
     * @return string
     */
    public function buildNodeName($node)
    {
        $result = $this->escapeHtml($node->getName());
        return $result;
    }

    /**
     * @param $node
     *
     * @return mixed
     */
    protected function _isElementMoveable($node)
    {
        $options = new Varien_Object(array(
            'is_moveable' => true,
            'element'     => $node
        ));

        Mage::dispatchEvent(
            'adminhtml_poebel_cmsnavigation_element_tree_is_moveable',
            array('options' => $options)
        );

        return $options->getIsMoveable();
    }

    /**
     * @param $node
     *
     * @return bool
     */
    protected function _isParentSelectedElement($node)
    {
        if ($node && $this->getElement()) {
            $pathIds = $this->getElement()->getPathIds();
            if (in_array($node->getId(), $pathIds)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isClearEdit()
    {
        return (bool)$this->getRequest()->getParam('clear');
    }

    /**
     * @return mixed
     */
    public function canAddRootElement()
    {
        $options = new Varien_Object(array('is_allow' => true));
        Mage::dispatchEvent(
            'adminhtml_poebel_cmsnavigation_element_tree_can_add_root_element',
            array(
                'element' => $this->getElement(),
                'options' => $options,
                'store'   => $this->getStore()->getId()
            )
        );

        return $options->getIsAllow();
    }

    /**
     * @return mixed
     */
    public function canAddSubElement()
    {
        $options = new Varien_Object(array('is_allow' => true));
        Mage::dispatchEvent(
            'adminhtml_poebel_cmsnavigation_element_tree_can_add_sub_element',
            array(
                'element' => $this->getElement(),
                'options' => $options,
                'store'   => $this->getStore()->getId()
            )
        );

        return $options->getIsAllow();
    }
}
