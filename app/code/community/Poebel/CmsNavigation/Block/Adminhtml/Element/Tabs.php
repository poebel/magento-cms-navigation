<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Element_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * @var string
     */
    protected $_attributeTabBlock = 'poebel_cmsnavigation/adminhtml_element_tab_attributes';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('element_info_tabs');
        $this->setDestElementId('element_tab_content');
        $this->setTitle(Mage::helper('poebel_cmsnavigation')->__('Element Data'));
        $this->setTemplate('widget/tabshoriz.phtml');
    }

    /**
     * @return mixed
     */
    public function getElement()
    {
        return Mage::registry('current_element');
    }

    /**
     * @return string
     */
    public function getAttributeTabBlock()
    {
        return $this->_attributeTabBlock;
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $elementAttributes = $this->getElement()->getAttributes();
        if (!$this->getElement()->getId()) {
            foreach ($elementAttributes as $attribute) {
                $default = $attribute->getDefaultValue();
                if ($default != '') {
                    $this->getElement()->setData($attribute->getAttributeCode(), $default);
                }
            }
        }

        $attributeSetId = $this->getElement()->getDefaultAttributeSetId();
        $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
            ->setAttributeSetFilter($attributeSetId)
            ->setSortOrder()
            ->load();
        $defaultGroupId  = 0;
        foreach ($groupCollection as $group) {
            if ($defaultGroupId == 0 or $group->getIsDefault()) {
                $defaultGroupId = $group->getId();
            }
        }

        foreach ($groupCollection as $group) {
            $attributes = array();
            foreach ($elementAttributes as $attribute) {
                if ($attribute->isInGroup($attributeSetId, $group->getId())) {
                    $attributes[] = $attribute;
                }
            }

            if (!$attributes) {
                continue;
            }

            $active = $defaultGroupId == $group->getId();
            $block = $this->getLayout()->createBlock($this->getAttributeTabBlock(), '')
                ->setGroup($group)
                ->setAttributes($attributes)
                ->setAddHiddenFields($active)
                ->toHtml();
            $this->addTab(
                'group_' . $group->getId(), array(
                    'label'   => Mage::helper('poebel_cmsnavigation')->__($group->getAttributeGroupName()),
                    'content' => $block,
                    'active'  => $active
                )
            );
        }

        Mage::dispatchEvent(
            'adminhtml_poebel_cmsnavigation_element_tabs', array(
                'tabs' => $this
            )
        );

        return parent::_prepareLayout();
    }
}
