<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Element_Abstract extends Mage_Adminhtml_Block_Template
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getElement()
    {
        return Mage::registry('element');
    }

    /**
     * @return int
     */
    public function getElementId()
    {
        if ($this->getElement()) {
            return $this->getElement()->getId();
        }
        return Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID;
    }

    /**
     * @return mixed
     */
    public function getElementName()
    {
        return $this->getElement()->getName();
    }

    /**
     * @return int
     */
    public function getElementPath()
    {
        if ($this->getElement()) {
            return $this->getElement()->getPath();
        }
        return Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID;
    }

    /**
     * @return bool
     */
    public function hasStoreRootElement()
    {
        $root = $this->getRoot();
        if ($root && $root->getId()) {
            return true;
        }
        return false;
    }

    /**
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store');
        return Mage::app()->getStore($storeId);
    }

    /**
     * @param null $parentNodeElement
     * @param int  $recursionLevel
     *
     * @return mixed
     */
    public function getRoot($parentNodeElement = null, $recursionLevel = 3)
    {
        if (!is_null($parentNodeElement) && $parentNodeElement->getId()) {
            return $this->getNode($parentNodeElement, $recursionLevel);
        }
        $root = Mage::registry('root_element');
        if (is_null($root)) {
            $storeId = (int)$this->getRequest()->getParam('store');

            if ($storeId) {
                $store  = Mage::app()->getStore($storeId);
                $rootId = 0;
                if ($store->getGroup()) {
                    $rootId = Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID, $store->getId());
                }
            } else {
                $rootId = Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID;
            }

            $tree = Mage::getResourceSingleton('poebel_cmsnavigation/element_tree')
                ->load(null, $recursionLevel);

            if ($this->getElement()) {
                $tree->loadEnsuredNodes($this->getElement(), $tree->getNodeById($rootId));
            }

            $tree->addCollectionData($this->getElementCollection());

            $root = $tree->getNodeById($rootId);

            if ($root && $rootId != Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID) {
                $root->setIsVisible(true);
            } elseif ($root && $root->getId() == Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID) {
                $root->setName(Mage::helper('poebel_cmsnavigation')->__('Root Element'));
            }

            Mage::register('root_element', $root);
        }

        return $root;
    }

    /**
     * @param $ids
     *
     * @return mixed
     */
    public function getRootByIds($ids)
    {
        $root = Mage::registry('root_element');
        if (null === $root) {
            $categoryTreeResource = Mage::getResourceSingleton('poebel_cmsnavigation/element_tree');
            $ids                  = $categoryTreeResource->getExistingElementIdsBySpecifiedIds($ids);
            $tree                 = $categoryTreeResource->loadByIds($ids);
            $rootId               = Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID;
            $root                 = $tree->getNodeById($rootId);
            if ($root && $rootId != Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID) {
                $root->setIsVisible(true);
            } else {
                if ($root && $root->getId() == Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID) {
                    $root->setName(Mage::helper('poebel_cmsnavigation')->__('Root Element'));
                }
            }

            $tree->addCollectionData($this->getElementCollection());
            Mage::register('root_element', $root);
        }
        return $root;
    }

    /**
     * @param     $parentNodeElement
     * @param int $recursionLevel
     *
     * @return mixed
     */
    public function getNode($parentNodeElement, $recursionLevel = 2)
    {
        $tree = Mage::getResourceModel('poebel_cmsnavigation/element_tree');

        $nodeId = $parentNodeElement->getId();

        $node = $tree->loadNode($nodeId);
        $node->loadChildren($recursionLevel);

        if ($node && $nodeId != Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID) {
            $node->setIsVisible(true);
        } elseif ($node && $node->getId() == Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID) {
            $node->setName(Mage::helper('poebel_cmsnavigation')->__('Root Element'));
        }

        $tree->addCollectionData($this->getElementCollection());

        return $node;
    }

    /**
     * @param array $args
     *
     * @return string
     */
    public function getSaveUrl(array $args = array())
    {
        $params = array('_current' => true);
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/save', $params);
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return $this->getUrl("*/cms_navigation/edit", array('_current' => true, 'store' => null, '_query' => false, 'id' => null, 'parent' => null));
    }

    /**
     * @return array|mixed
     */
    public function getRootIds()
    {
        $ids = $this->getData('root_ids');
        if (is_null($ids)) {
            $ids = array();
            foreach (Mage::app()->getGroups() as $store) {
                $ids[] = Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID, $store->getId());
            }
            $this->setData('root_ids', $ids);
        }
        return $ids;
    }
}
