<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Element_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     *
     */
    public function __construct()
    {
        $this->_objectId   = 'entity_id';
        $this->_blockGroup = 'poebel_cmsnavigation';
        $this->_controller = 'adminhtml_element';
        $this->_mode       = 'edit';

        parent::__construct();
        $this->setTemplate('poebel/cmsnavigation/element/edit.phtml');
    }
}
