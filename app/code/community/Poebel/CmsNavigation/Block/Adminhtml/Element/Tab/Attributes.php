<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Element_Tab_Attributes extends Poebel_CmsNavigation_Block_Adminhtml_Form
{
    /**
     * @return mixed
     */
    public function getElement()
    {
        return Mage::registry('current_element');
    }

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setShowGlobalIcon(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $group      = $this->getGroup();
        $attributes = $this->getAttributes();

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('group_' . $group->getId());
        $form->setDataObject($this->getElement());

        $fieldset = $form->addFieldset(
            'fieldset_group_' . $group->getId(), array(
                'legend' => Mage::helper('poebel_cmsnavigation')->__($group->getAttributeGroupName()),
                'class'  => 'fieldset-wide',
            )
        );

        if ($this->getAddHiddenFields()) {
            if (!$this->getElement()->getId()) {
                // path
                if ($this->getRequest()->getParam('parent')) {
                    $fieldset->addField(
                        'path', 'hidden', array(
                            'name'  => 'path',
                            'value' => $this->getRequest()->getParam('parent')
                        )
                    );
                } else {
                    $fieldset->addField(
                        'path', 'hidden', array(
                            'name'  => 'path',
                            'value' => 1
                        )
                    );
                }
            } else {
                $fieldset->addField(
                    'id', 'hidden', array(
                        'name'  => 'id',
                        'value' => $this->getElement()->getId()
                    )
                );
                $fieldset->addField(
                    'path', 'hidden', array(
                        'name'  => 'path',
                        'value' => $this->getElement()->getPath()
                    )
                );
            }
        }

        $this->_setFieldset($attributes, $fieldset);

        if ($this->getElement()->getLevel() == 1) {
            $fieldset->removeField('custom_use_parent_settings');
        } else {
            if ($this->getElement()->getCustomUseParentSettings()) {
                foreach ($this->getElement()->getDesignAttributes() as $attribute) {
                    if ($element = $form->getElement($attribute->getAttributeCode())) {
                        $element->setDisabled(true);
                    }
                }
            }
            if ($element = $form->getElement('custom_use_parent_settings')) {
                $element->setData('onchange', 'onCustomUseParentChanged(this)');
            }
        }

        if ($this->getElement()->hasLockedAttributes()) {
            foreach ($this->getElement()->getLockedAttributes() as $attribute) {
                if ($element = $form->getElement($attribute)) {
                    $element->setReadonly(true, true);
                }
            }
        }

        if (!$this->getElement()->getId()) {
            $this->getElement()->setIsActive(1);
        }

        $form->addValues($this->getElement()->getData());

        Mage::dispatchEvent('adminhtml_poebel_cmsnavigation_element_edit_prepare_form', array('form' => $form));

        $form->setFieldNameSuffix('general');
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
