<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Element_Edit_Form extends Poebel_CmsNavigation_Block_Adminhtml_Element_Abstract
{
    /**
     * @var array
     */
    protected $_additionalButtons = array();

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('poebel/cmsnavigation/element/edit/form.phtml');
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $element   = $this->getElement();
        $elementId = (int)$element->getId();

        $this->setChild(
            'tabs',
            $this->getLayout()->createBlock('poebel_cmsnavigation/adminhtml_element_tabs', 'tabs')
        );

        // Save button
        if (!$element->isReadonly()) {
            $this->setChild(
                'save_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(
                        array(
                            'label'   => Mage::helper('catalog')->__('Save Element'),
                            'onclick' => "elementSubmit('" . $this->getSaveUrl() . "', true)",
                            'class'   => 'save'
                        )
                    )
            );
        }

        // Delete button
        if (!in_array($elementId, $this->getRootIds()) && $element->isDeleteable()) {
            $this->setChild(
                'delete_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(
                        array(
                            'label'   => Mage::helper('catalog')->__('Delete Element'),
                            'onclick' => "elementDelete('" . $this->getUrl('*/*/delete', array('_current' => true)) . "', true, {$elementId})",
                            'class'   => 'delete'
                        )
                    )
            );
        }

        // Reset button
        if (!$element->isReadonly()) {
            $resetPath = $elementId ? '*/*/edit' : '*/*/add';
            $this->setChild(
                'reset_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(
                        array(
                            'label'   => Mage::helper('catalog')->__('Reset'),
                            'onclick' => "elementReset('" . $this->getUrl($resetPath, array('_current' => true)) . "',true)"
                        )
                    )
            );
        }

        return parent::_prepareLayout();
    }

    /**
     * @return string
     */
    public function getStoreConfigurationUrl()
    {
        $params = array(
            'section' => 'cms',
        );

        if ($storeId = (int)$this->getRequest()->getParam('store')) {
            $store             = Mage::app()->getStore($storeId);
            $params['website'] = $store->getWebsite()->getCode();
        }

        return $this->getUrl('*/system_config/edit', $params);
    }

    /**
     * @return string
     */
    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

    /**
     * @return string
     */
    public function getSaveButtonHtml()
    {
        if ($this->hasStoreRootElement()) {
            return $this->getChildHtml('save_button');
        }
        return '';
    }

    /**
     * @return string
     */
    public function getResetButtonHtml()
    {
        if ($this->hasStoreRootElement()) {
            return $this->getChildHtml('reset_button');
        }
        return '';
    }

    /**
     * @return string
     */
    public function getAdditionalButtonsHtml()
    {
        $html = '';
        foreach ($this->_additionalButtons as $childName) {
            $html .= $this->getChildHtml($childName);
        }
        return $html;
    }

    /**
     * @param $alias
     * @param $config
     *
     * @return $this
     */
    public function addAdditionalButton($alias, $config)
    {
        if (isset($config['name'])) {
            $config['element_name'] = $config['name'];
        }
        $this->setChild(
            $alias . '_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')->addData($config)
        );
        $this->_additionalButtons[$alias] = $alias . '_button';
        return $this;
    }

    /**
     * @param $alias
     *
     * @return $this
     */
    public function removeAdditionalButton($alias)
    {
        if (isset($this->_additionalButtons[$alias])) {
            $this->unsetChild($this->_additionalButtons[$alias]);
            unset($this->_additionalButtons[$alias]);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getTabsHtml()
    {
        return $this->getChildHtml('tabs');
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        if ($this->hasStoreRootElement()) {
            if ($this->getElementId()) {
                return $this->getElementName();
            } else {
                $parentId = (int)$this->getRequest()->getParam('parent');
                if ($parentId && ($parentId != Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID)) {
                    return Mage::helper('poebel_cmsnavigation')->__('New Sub Element');
                } else {
                    return Mage::helper('poebel_cmsnavigation')->__('New Root Element');
                }
            }
        }
        return Mage::helper('poebel_cmsnavigation')->__('Set Root Element for Store');
    }

    /**
     * @param array $args
     *
     * @return string
     */
    public function getDeleteUrl(array $args = array())
    {
        $params = array('_current' => true);
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/delete', $params);
    }

    /**
     * @param array $args
     *
     * @return string
     */
    public function getRefreshPathUrl(array $args = array())
    {
        $params = array('_current' => true);
        $params = array_merge($params, $args);
        return $this->getUrl('*/*/refreshPath', $params);
    }

    /**
     * @return bool
     */
    public function isAjax()
    {
        return Mage::app()->getRequest()->isXmlHttpRequest() || Mage::app()->getRequest()->getParam('isAjax');
    }
}
