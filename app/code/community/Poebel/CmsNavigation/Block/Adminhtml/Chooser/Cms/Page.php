<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Block_Adminhtml_Chooser_Cms_Page extends Mage_Adminhtml_Block_Cms_Page_Widget_Chooser
{
    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/cms_navigation/chooser', array('_current' => true, 'uniq_id' => $this->getElementId(), 'type' => $this->getChooserType()));
    }

    /**
     * @return string
     */
    public function getRowClickCallback()
    {
        $js
            = 'function (grid, event) {
                var trElement = Event.findElement(event, "tr");
                var pageTitle = trElement.down("td").next().innerHTML;
                var pageId = trElement.down("td").next().next().innerHTML.replace(/^\s+|\s+$/g,"");
                $("' . $this->getElementId() . '").value = pageId;
                $("' . $this->getElementId() . '_label").update("' . $this->__('Selected page:') . ' " + pageTitle + " (" + pageId + ")");
                chooser.closeDialog();
            }';

        return $js;
    }
}