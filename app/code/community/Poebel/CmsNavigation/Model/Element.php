<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Model_Element extends Poebel_CmsNavigation_Model_Abstract
{
    const ENTITY = 'poebel_cmsnavigation_element';

    const MODE_CMS_PAGE = 1;
    const MODE_CATEGORY = 2;
    const MODE_PRODUCT  = 3;
    const MODE_URL      = 4;
    const MODE_ACTION   = 5;

    const TREE_ROOT_ID = 1;

    const CACHE_TAG = 'poebel_cmsnavigation_element';

    /**
     * @var string
     */
    protected $_eventPrefix = 'poebel_cmsnavigation';
    /**
     * @var string
     */
    protected $_eventObject = 'element';

    /**
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * @var null
     */
    protected $_treeModel = null;

    /**
     *
     */
    function _construct()
    {
        $this->_init('poebel_cmsnavigation/element');
    }

    /**
     * @return mixed
     */
    public function getTreeModel()
    {
        return Mage::getResourceModel('poebel_cmsnavigation/element_tree');
    }

    /**
     * @return null
     */
    public function getTreeModelInstance()
    {
        if (is_null($this->_treeModel)) {
            $this->_treeModel = Mage::getResourceSingleton('poebel_cmsnavigation/element_tree');
        }
        return $this->_treeModel;
    }

    /**
     * @param $parentId
     * @param $afterElementId
     *
     * @return $this
     * @throws Exception
     */
    public function move($parentId, $afterElementId)
    {

        $parent = Mage::getModel('poebel_cmsnavigation/element')
            ->setStoreId($this->getStoreId())
            ->load($parentId);

        if (!$parent->getId()) {
            Mage::throwException(
                Mage::helper('poebel_cmsnavigation')->__('Element move operation is not possible: the new parent element was not found.')
            );
        }

        if (!$this->getId()) {
            Mage::throwException(
                Mage::helper('poebel_cmsnavigation')->__('Element move operation is not possible: the current element was not found.')
            );
        } elseif ($parent->getId() == $this->getId()) {
            Mage::throwException(
                Mage::helper('poebel_cmsnavigation')->__('Element move operation is not possible: parent element is equal to child element.')
            );
        }

        $this->setMovedElementId($this->getId());

        $eventParams  = array(
            $this->_eventObject => $this,
            'parent'            => $parent,
            'category_id'       => $this->getId(),
            'prev_parent_id'    => $this->getParentId(),
            'parent_id'         => $parentId
        );
        $moveComplete = false;

        $this->_getResource()->beginTransaction();
        try {
            Mage::dispatchEvent('poebel_cmsnavigation_element_tree_move_before', $eventParams);
            Mage::dispatchEvent($this->_eventPrefix . '_move_before', $eventParams);

            $this->getResource()->changeParent($this, $parent, $afterElementId);

            Mage::dispatchEvent($this->_eventPrefix . '_move_after', $eventParams);
            Mage::dispatchEvent('poebel_cmsnavigation_element_tree_move_after', $eventParams);
            $this->_getResource()->commit();

            $moveComplete = true;
        } catch (Exception $e) {
            $this->_getResource()->rollBack();
            throw $e;
        }
        if ($moveComplete) {
            Mage::dispatchEvent('poebel_cmsnavigation_element_move', $eventParams);
            Mage::app()->cleanCache(array(self::CACHE_TAG));
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefaultAttributeSetId()
    {
        return $this->getResource()->getEntityType()->getDefaultAttributeSetId();
    }

    /**
     * @return mixed
     */
    public function getAttributes()
    {
        $result = $this->getResource()
            ->loadAllAttributes($this)
            ->getSortedAttributes();

        return $result;
    }

    /**
     * @return array|mixed
     */
    public function getStoreIds()
    {
        if ($this->getInitialSetupFlag()) {
            return array();
        }

        if ($storeIds = $this->getData('store_ids')) {
            return $storeIds;
        }

        if (!$this->getId()) {
            return array();
        }

        $nodes = array();
        foreach ($this->getPathIds() as $id) {
            $nodes[] = $id;
        }

        $storeIds = array();
        foreach (Mage::app()->getStores() as $store) {
            if (in_array(Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID, $store->getId()), $nodes)) {
                $storeIds[$store->getId()] = $store->getId();
            }
        }

        $entityStoreId = $this->getStoreId();
        if (!in_array($entityStoreId, $storeIds)) {
            array_unshift($storeIds, $entityStoreId);
        }
        if (!in_array(0, $storeIds)) {
            array_unshift($storeIds, 0);
        }

        $this->setData('store_ids', $storeIds);
        return $storeIds;
    }

    /**
     * @return int|mixed
     */
    public function getStoreId()
    {
        if ($this->hasData('store_id')) {
            return $this->_getData('store_id');
        }
        return Mage::app()->getStore()->getId();
    }

    /**
     * @param $storeId
     *
     * @return $this
     */
    public function setStoreId($storeId)
    {
        if (!is_numeric($storeId)) {
            $storeId = Mage::app($storeId)->getStore()->getId();
        }
        $this->setData('store_id', $storeId);
        $this->getResource()->setStoreId($storeId);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentElement()
    {
        if (!$this->hasData('parent_element')) {
            $this->setData('parent_element', Mage::getModel('poebel_cmsnavigation/element')->load($this->getParentId()));
        }
        return $this->_getData('parent_element');
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        $parentIds = $this->getParentIds();
        return intval(array_pop($parentIds));
    }

    /**
     * @return array
     */
    public function getParentIds()
    {
        return array_diff($this->getPathIds(), array($this->getId()));
    }

    /**
     * @param bool $asArray
     *
     * @return string
     */
    public function getAllChildren($asArray = false)
    {
        $children = $this->getResource()->getAllChildren($this);
        if ($asArray) {
            return $children;
        } else {
            return implode(',', $children);
        }
    }

    /**
     * @return string
     */
    public function getChildren()
    {
        return implode(',', $this->getResource()->getChildren($this, false));
    }

    /**
     * @return string
     */
    public function getPathInStore()
    {
        $rootElementId = 0;

        if (Mage::app()->getStore()->getGroup()) {
            $rootElementId = Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID);
        }

        $result = array();
        $path   = array_reverse($this->getPathIds());
        foreach ($path as $itemId) {
            if ($itemId == $rootElementId) {
                break;
            }
            $result[] = $itemId;
        }
        return implode(',', $result);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function checkId($id)
    {
        return $this->_getResource()->checkId($id);
    }

    /**
     * @return array|mixed
     */
    public function getPathIds()
    {
        $ids = $this->getData('path_ids');
        if (is_null($ids)) {
            $ids = explode('/', $this->getPath());
            $this->setData('path_ids', $ids);
        }
        return $ids;
    }

    /**
     * @return int|mixed
     */
    public function getLevel()
    {
        if (!$this->hasLevel()) {
            return count(explode('/', $this->getPath())) - 1;
        }
        return $this->getData('level');
    }

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function verifyIds(array $ids)
    {
        return $this->getResource()->verifyIds($ids);
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return $this->_getResource()->getChildrenAmount($this) > 0;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_getData('name');
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeDelete()
    {
        $this->_protectFromNonAdmin();
        if ($this->getResource()->isForbiddenToDelete($this->getId())) {
            Mage::throwException("Can't delete root element.");
        }
        return parent::_beforeDelete();
    }

    /**
     * @param      $parent
     * @param int  $recursionLevel
     * @param bool $sorted
     * @param bool $asCollection
     * @param bool $toLoad
     *
     * @return mixed
     */
    public function getElements($parent, $recursionLevel = 0, $sorted = false, $asCollection = false, $toLoad = true)
    {
        $elements = $this->getResource()
            ->getElements($parent, $recursionLevel, $sorted, $asCollection, $toLoad);
        return $elements;
    }

    /**
     * @return mixed
     */
    public function getParentElements()
    {
        return $this->getResource()->getParentElements($this);
    }

    /**
     * @return mixed
     */
    public function getChildrenElements()
    {
        return $this->getResource()->getChildrenElements($this);
    }

    /**
     * @return mixed
     */
    public function getChildrenElementsWithInactive()
    {
        return $this->getResource()->getChildrenElementsWithInactive($this);
    }

    /**
     * @return mixed
     */
    public function isInRootElementList()
    {
        return $this->getResource()->isInRootElementList($this);
    }

    /**
     * @return mixed
     */
    public function validate()
    {
        return $this->_getResource()->validate($this);
    }
}