<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function addCmsNavigationToTopmenuItems(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();
        $block->addCacheTag(Poebel_CmsNavigation_Model_Element::CACHE_TAG);
        $this->_addElementsToMenu(Mage::helper('poebel_cmsnavigation/element')->getStoreElements(), $observer->getMenu(), $block, true);
    }

    /**
     * @param      $elements
     * @param      $parentElementNode
     * @param      $menuBlock
     * @param bool $addTags
     *
     * @return bool
     */
    protected function _addElementsToMenu($elements, $parentElementNode, $menuBlock, $addTags = false)
    {
        $hasActiveChild = false;

        $elementModel = Mage::getModel('poebel_cmsnavigation/element');
        foreach ($elements as $element) {
            if (!$element->getIsActive()) {
                continue;
            }

            $nodeId = 'element-node-' . $element->getId();

            $elementModel->setId($element->getId());
            if ($addTags) {
                $menuBlock->addModelTags($elementModel);
            }

            $isActive = Mage::helper('poebel_cmsnavigation/element')->isActiveMenuElement($element);

            if ($isActive) {
                $hasActiveChild = true;
            }

            $tree        = $parentElementNode->getTree();
            $elementData = array(
                'name'      => $element->getName(),
                'id'        => $nodeId,
                'url'       => Mage::helper('poebel_cmsnavigation/element')->getElementUrl($element),
                'is_active' => $isActive,
            );
            $elementNode = new Varien_Data_Tree_Node($elementData, 'id', $tree, $parentElementNode);
            $parentElementNode->addChild($elementNode);

            $subelements = $element->getChildren();

            if ($this->_addElementsToMenu($subelements, $elementNode, $menuBlock, $addTags)) {
                $hasActiveChild = true;
                if (Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_PARENT_IS_ACTIVE)) {
                    $elementNode->setIsActive(true);
                }
            }
        }

        return $hasActiveChild;
    }
}