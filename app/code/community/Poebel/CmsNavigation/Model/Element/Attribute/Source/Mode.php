<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Model_Element_Attribute_Source_Mode extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = array(
                array(
                    'value' => Poebel_CmsNavigation_Model_Element::MODE_CMS_PAGE,
                    'label' => Mage::helper('poebel_cmsnavigation')->__('CMS Page'),
                ),
                array(
                    'value' => Poebel_CmsNavigation_Model_Element::MODE_ACTION,
                    'label' => Mage::helper('poebel_cmsnavigation')->__('Controller Action'),
                ),
                array(
                    'value' => Poebel_CmsNavigation_Model_Element::MODE_URL,
                    'label' => Mage::helper('poebel_cmsnavigation')->__('Static URL'),
                ),
            );

            if(Mage::helper('core')->isModuleEnabled('Mage_Catalog')) {
                $this->_options = array_merge($this->_options, array(
                        array(
                            'value' => Poebel_CmsNavigation_Model_Element::MODE_CATEGORY,
                            'label' => Mage::helper('poebel_cmsnavigation')->__('Category'),
                        ),
                        array(
                            'value' => Poebel_CmsNavigation_Model_Element::MODE_PRODUCT,
                            'label' => Mage::helper('poebel_cmsnavigation')->__('Product'),
                        ),
                    ));
            }

            array_unshift(
                $this->_options, array(
                    'value' => '',
                    'label' => Mage::helper('poebel_cmsnavigation')->__('-- Choose Mode --')
                )
            );
        }
        return $this->_options;
    }
}
