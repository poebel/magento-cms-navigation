<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup
{
    /**
     * @param array $attr
     *
     * @return array
     */
    protected function _prepareValues($attr)
    {
        $data = parent::_prepareValues($attr);
        $data = array_merge(
            $data, array(
                'is_global'                => $this->_getValue(
                    $attr,
                    'global',
                    Poebel_CmsNavigation_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
                ),
                'is_visible'               => $this->_getValue($attr, 'visible', 1),
                'is_visible_on_front'      => $this->_getValue($attr, 'visible_on_front', 0),
                'is_wysiwyg_enabled'       => $this->_getValue($attr, 'wysiwyg_enabled', 0),
                'is_html_allowed_on_front' => $this->_getValue($attr, 'is_html_allowed_on_front', 0),
                'position'                 => $this->_getValue($attr, 'position', 0),
            )
        );
        return $data;
    }

    /**
     * @return array
     */
    public function getDefaultEntities()
    {
        return array(
            Poebel_CmsNavigation_Model_Element::ENTITY => array(
                'entity_model'                => 'poebel_cmsnavigation/element',
                'attribute_model'             => 'poebel_cmsnavigation/resource_eav_attribute',
                'table'                       => 'poebel_cmsnavigation/element',
                'additional_attribute_table'  => 'poebel_cmsnavigation/eav_attribute',
                'entity_attribute_collection' => 'poebel_cmsnavigation/eav_attribute_collection',
                'default_group'               => 'General',
                'attributes'                  => array(
                    'name'           => array(
                        'type'       => 'varchar',
                        'label'      => 'Name',
                        'input'      => 'text',
                        'sort_order' => 100,
                        'global'     => Poebel_CmsNavigation_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'group'      => 'General',
                    ),
                    'is_active'      => array(
                        'type'       => 'int',
                        'label'      => 'Is Active',
                        'input'      => 'select',
                        'source'     => 'eav/entity_attribute_source_boolean',
                        'sort_order' => 200,
                        'global'     => Poebel_CmsNavigation_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'group'      => 'General',
                    ),
                    'mode'           => array(
                        'type'       => 'int',
                        'label'      => 'Mode',
                        'input'      => 'select',
                        'source'     => 'poebel_cmsnavigation/element_attribute_source_mode',
                        'required'   => false,
                        'sort_order' => 300,
                        'global'     => Poebel_CmsNavigation_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'group'      => 'General',
                    ),
                    'cms_page_id'    => array(
                        'type'       => 'varchar',
                        'label'      => 'Target CMS Page',
                        'input'      => 'select',
                        'source'     => 'poebel_cmsnavigation/element_attribute_source_page',
                        'required'   => false,
                        'sort_order' => 400,
                        'global'     => Poebel_CmsNavigation_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'group'      => 'General',
                    ),
                    'action'         => array(
                        'type'       => 'varchar',
                        'label'      => 'Target Action',
                        'input'      => 'text',
                        'required'   => false,
                        'sort_order' => 410,
                        'global'     => Poebel_CmsNavigation_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'group'      => 'General',
                    ),
                    'url'            => array(
                        'type'       => 'text',
                        'label'      => 'Target URL',
                        'input'      => 'text',
                        'required'   => false,
                        'sort_order' => 420,
                        'global'     => Poebel_CmsNavigation_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'group'      => 'General',
                    ),
                    'category_id'    => array(
                        'type'       => 'int',
                        'label'      => 'Target Category',
                        'input'      => 'select',
                        'source'     => 'poebel_cmsnavigation/element_attribute_source_category',
                        'required'   => false,
                        'sort_order' => 430,
                        'global'     => Poebel_CmsNavigation_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'group'      => 'General',
                    ),
                    'product_id'     => array(
                        'type'       => 'int',
                        'label'      => 'Target Product',
                        'input'      => 'select',
                        'source'     => 'poebel_cmsnavigation/element_attribute_source_product',
                        'required'   => false,
                        'sort_order' => 440,
                        'global'     => Poebel_CmsNavigation_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'group'      => 'General',
                    ),
                    'path'           => array(
                        'type'       => 'static',
                        'label'      => 'Path',
                        'required'   => false,
                        'sort_order' => 500,
                        'visible'    => false,
                        'group'      => 'General',
                    ),
                    'position'       => array(
                        'type'       => 'static',
                        'label'      => 'Position',
                        'required'   => false,
                        'sort_order' => 500,
                        'visible'    => false,
                        'group'      => 'General',
                    ),
                    'all_children'   => array(
                        'type'       => 'text',
                        'label'      => 'All Children',
                        'required'   => false,
                        'sort_order' => 500,
                        'visible'    => false,
                        'group'      => 'General',
                    ),
                    'path_in_store'  => array(
                        'type'       => 'text',
                        'label'      => 'Path in Store',
                        'required'   => false,
                        'sort_order' => 500,
                        'visible'    => false,
                        'group'      => 'General',
                    ),
                    'children'       => array(
                        'type'       => 'text',
                        'label'      => 'Children',
                        'required'   => false,
                        'sort_order' => 500,
                        'visible'    => false,
                        'group'      => 'General',
                    ),
                    'level'          => array(
                        'type'       => 'static',
                        'label'      => 'Level',
                        'required'   => false,
                        'sort_order' => 500,
                        'visible'    => false,
                        'group'      => 'General',
                    ),
                    'children_count' => array(
                        'type'       => 'static',
                        'label'      => 'Children Count',
                        'required'   => false,
                        'sort_order' => 500,
                        'visible'    => false,
                        'group'      => 'General',
                    ),
                )
            )
        );
    }

    /**
     * @param $entityId
     *
     * @return array
     */
    protected function _getElementEntityRow($entityId)
    {
        $select = $this->getConnection()->select();

        $select->from($this->getTable('poebel_cmsnavigation/element'));
        $select->where('entity_id = :entity_id');

        return $this->getConnection()->fetchRow($select, array('entity_id' => $entityId));
    }

    /**
     * @param       $element
     * @param array $path
     *
     * @return array
     */
    protected function _getElementPath($element, $path = array())
    {
        $path[] = $element['entity_id'];

        if ($element['parent_id'] != 0) {
            $parentElement = $this->_getElementEntityRow($element['parent_id']);
            if ($parentElement) {
                $path = $this->_getElementPath($parentElement, $path);
            }
        }

        return $path;
    }
}