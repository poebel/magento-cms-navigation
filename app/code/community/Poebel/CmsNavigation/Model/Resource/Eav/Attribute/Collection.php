<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Model_Resource_Eav_Attribute_Collection extends Mage_Eav_Model_Resource_Entity_Attribute_Collection
{
    /**
     * @return $this
     */
    protected function _initSelect()
    {
        $this->getSelect()->from(array('main_table' => $this->getResource()->getMainTable()))
            ->where('main_table.entity_type_id=?', Mage::getModel('eav/entity')->setType(Poebel_CmsNavigation_Model_Element::ENTITY)->getTypeId())
            ->join(
                array('additional_table' => $this->getTable('poebel_cmsnavigation/eav_attribute')),
                'additional_table.attribute_id = main_table.attribute_id'
            );
        return $this;
    }

    /**
     * @param int|Mage_Eav_Model_Entity_Type $typeId
     *
     * @return $this
     */
    public function setEntityTypeFilter($typeId)
    {
        return $this;
    }
}
