<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Model_Resource_Element extends Poebel_CmsNavigation_Model_Resource_Abstract
{
    /**
     * @var
     */
    protected $_tree;
    /**
     * @var null
     */
    protected $_isActiveAttributeId = null;
    /**
     * @var null
     */
    protected $_storeId = null;

    /**
     *
     */
    public function __construct()
    {
        $resource = Mage::getSingleton('core/resource');

        $this->setType(Poebel_CmsNavigation_Model_Element::ENTITY);

        $this->setConnection(
            $resource->getConnection('poebel_cmsnavigation_read'),
            $resource->getConnection('poebel_cmsnavigation_write')
        );
    }

    /**
     * @param $storeId
     *
     * @return $this
     */
    public function setStoreId($storeId)
    {
        $this->_storeId = $storeId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStoreId()
    {
        if ($this->_storeId === null) {
            return Mage::app()->getStore()->getId();
        }
        return $this->_storeId;
    }

    /**
     * @return mixed
     */
    protected function _getTree()
    {
        if (!$this->_tree) {
            $this->_tree = Mage::getResourceModel('poebel_cmsnavigation/element_tree')->load();
        }
        return $this->_tree;
    }

    /**
     * @param Varien_Object $object
     *
     * @return $this
     */
    protected function _beforeDelete(Varien_Object $object)
    {
        parent::_beforeDelete($object);

        $parentIds = $object->getParentIds();
        if ($parentIds) {
            $childDecrease = $object->getChildrenCount() + 1;
            $data          = array('children_count' => new Zend_Db_Expr('children_count - ' . $childDecrease));
            $where         = array('entity_id IN(?)' => $parentIds);
            $this->_getWriteAdapter()->update($this->getEntityTable(), $data, $where);
        }
        $this->deleteChildren($object);
        return $this;
    }

    /**
     * @param Varien_Object $object
     *
     * @return $this
     */
    public function deleteChildren(Varien_Object $object)
    {
        $adapter   = $this->_getWriteAdapter();
        $pathField = $adapter->quoteIdentifier('path');

        $select = $adapter->select()
            ->from($this->getEntityTable(), array('entity_id'))
            ->where($pathField . ' LIKE :c_path');

        $childrenIds = $adapter->fetchCol($select, array('c_path' => $object->getPath() . '/%'));

        if (!empty($childrenIds)) {
            $adapter->delete(
                $this->getEntityTable(),
                array('entity_id IN (?)' => $childrenIds)
            );
        }

        $object->setDeletedChildrenIds($childrenIds);
        return $this;
    }

    /**
     * @param Varien_Object $object
     *
     * @return $this
     */
    protected function _beforeSave(Varien_Object $object)
    {
        parent::_beforeSave($object);

        if (!$object->getChildrenCount()) {
            $object->setChildrenCount(0);
        }
        if ($object->getLevel() === null) {
            $object->setLevel(1);
        }

        if (!$object->getId()) {
            $object->setPosition($this->_getMaxPosition($object->getPath()) + 1);
            $path  = explode('/', $object->getPath());
            $level = count($path);
            $object->setLevel($level);
            if ($level) {
                $object->setParentId($path[$level - 1]);
            }
            $object->setPath($object->getPath() . '/');

            $toUpdateChild = explode('/', $object->getPath());

            $this->_getWriteAdapter()->update(
                $this->getEntityTable(),
                array('children_count' => new Zend_Db_Expr('children_count+1')),
                array('entity_id IN(?)' => $toUpdateChild)
            );
        }
        return $this;
    }

    /**
     * @param Varien_Object $object
     *
     * @return Mage_Eav_Model_Entity_Abstract
     */
    protected function _afterSave(Varien_Object $object)
    {
        if (substr($object->getPath(), -1) == '/') {
            $object->setPath($object->getPath() . $object->getId());
            $this->_savePath($object);
        }

        return parent::_afterSave($object);
    }

    /**
     * @param $object
     *
     * @return $this
     */
    protected function _savePath($object)
    {
        if ($object->getId()) {
            $this->_getWriteAdapter()->update(
                $this->getEntityTable(),
                array('path' => $object->getPath()),
                array('entity_id = ?' => $object->getId())
            );
        }
        return $this;
    }

    /**
     * @param $path
     *
     * @return int|string
     */
    protected function _getMaxPosition($path)
    {
        $adapter       = $this->getReadConnection();
        $positionField = $adapter->quoteIdentifier('position');
        $level         = count(explode('/', $path));
        $bind          = array(
            'c_level' => $level,
            'c_path'  => $path . '/%'
        );
        $select        = $adapter->select()
            ->from($this->getTable('poebel_cmsnavigation/element'), 'MAX(' . $positionField . ')')
            ->where($adapter->quoteIdentifier('path') . ' LIKE :c_path')
            ->where($adapter->quoteIdentifier('level') . ' = :c_level');

        $position = $adapter->fetchOne($select, $bind);
        if (!$position) {
            $position = 0;
        }
        return $position;
    }

    /**
     * @param $elementId
     *
     * @return string
     */
    public function getChildrenCount($elementId)
    {
        $select = $this->_getReadAdapter()->select()
            ->from($this->getEntityTable(), 'children_count')
            ->where('entity_id = :entity_id');
        $bind   = array('entity_id' => $elementId);

        return $this->_getReadAdapter()->fetchOne($select, $bind);
    }

    /**
     * @param $entityId
     *
     * @return string
     */
    public function checkId($entityId)
    {
        $select = $this->_getReadAdapter()->select()
            ->from($this->getEntityTable(), 'entity_id')
            ->where('entity_id = :entity_id');
        $bind   = array('entity_id' => $entityId);

        return $this->_getReadAdapter()->fetchOne($select, $bind);
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function verifyIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }

        $select = $this->_getReadAdapter()->select()
            ->from($this->getEntityTable(), 'entity_id')
            ->where('entity_id IN(?)', $ids);

        return $this->_getReadAdapter()->fetchCol($select);
    }

    /**
     * @param      $element
     * @param bool $isActiveFlag
     *
     * @return string
     */
    public function getChildrenAmount($element, $isActiveFlag = true)
    {
        $storeId     = Mage::app()->getStore()->getId();
        $attributeId = $this->_getIsActiveAttributeId();
        $table       = $this->getTable(array($this->getEntityTablePrefix(), 'int'));
        $adapter     = $this->_getReadAdapter();
        $checkSql    = $adapter->getCheckSql('c.value_id > 0', 'c.value', 'd.value');

        $bind   = array(
            'attribute_id' => $attributeId,
            'store_id'     => $storeId,
            'active_flag'  => $isActiveFlag,
            'c_path'       => $element->getPath() . '/%'
        );
        $select = $adapter->select()
            ->from(array('m' => $this->getEntityTable()), array('COUNT(m.entity_id)'))
            ->joinLeft(
                array('d' => $table),
                'd.attribute_id = :attribute_id AND d.store_id = 0 AND d.entity_id = m.entity_id',
                array()
            )
            ->joinLeft(
                array('c' => $table),
                "c.attribute_id = :attribute_id AND c.store_id = :store_id AND c.entity_id = m.entity_id",
                array()
            )
            ->where('m.path LIKE :c_path')
            ->where($checkSql . ' = :active_flag');

        return $this->_getReadAdapter()->fetchOne($select, $bind);
    }

    /**
     * @return null|string
     */
    protected function _getIsActiveAttributeId()
    {
        if ($this->_isActiveAttributeId === null) {
            $bind   = array(
                'poebel_cmsnavigation_element' => Poebel_CmsNavigation_Model_Element::ENTITY,
                'is_active'                    => 'is_active',
            );
            $select = $this->_getReadAdapter()->select()
                ->from(array('a' => $this->getTable('eav/attribute')), array('attribute_id'))
                ->join(array('t' => $this->getTable('eav/entity_type')), 'a.entity_type_id = t.entity_type_id')
                ->where('entity_type_code = :poebel_cmsnavigation_element')
                ->where('attribute_code = :is_active');

            $this->_isActiveAttributeId = $this->_getReadAdapter()->fetchOne($select, $bind);
        }

        return $this->_isActiveAttributeId;
    }

    /**
     * @param $entityIdsFilter
     * @param $attribute
     * @param $expectedValue
     *
     * @return array
     */
    public function findWhereAttributeIs($entityIdsFilter, $attribute, $expectedValue)
    {
        $bind   = array(
            'attribute_id' => $attribute->getId(),
            'value'        => $expectedValue
        );
        $select = $this->_getReadAdapter()->select()
            ->from($attribute->getBackend()->getTable(), array('entity_id'))
            ->where('attribute_id = :attribute_id')
            ->where('value = :value')
            ->where('entity_id IN(?)', $entityIdsFilter);

        return $this->_getReadAdapter()->fetchCol($select, $bind);
    }

    /**
     * @param      $parent
     * @param int  $recursionLevel
     * @param bool $sorted
     * @param bool $asCollection
     * @param bool $toLoad
     *
     * @return Varien_Data_Tree_Node_Collection
     */
    public function getElements($parent, $recursionLevel = 0, $sorted = false, $asCollection = false, $toLoad = true)
    {
        /** @var Poebel_CmsNavigation_Model_Resource_Element_Tree $tree */
        $tree  = Mage::getResourceModel('poebel_cmsnavigation/element_tree');
        $nodes = $tree->loadNode($parent)
            ->loadChildren($recursionLevel)
            ->getChildren();

        $tree->addCollectionData(null, $sorted, $parent, $toLoad, true);

        if ($asCollection) {
            return $tree->getCollection();
        }
        return $nodes;
    }

    /**
     * @param $element
     *
     * @return mixed
     */
    public function getParentElements($element)
    {
        $pathIds  = array_reverse(explode(',', $element->getPathInStore()));
        $elements = Mage::getResourceModel('poebel_cmsnavigation/element_collection')
            ->setStore(Mage::app()->getStore())
            ->addAttributeToSelect('name')
            ->addFieldToFilter('entity_id', array('in' => $pathIds))
            ->addFieldToFilter('is_active', 1)
            ->load()
            ->getItems();
        return $elements;
    }

    /**
     * @param $element
     *
     * @return mixed
     */
    protected function _getChildrenElementsBase($element)
    {
        $collection = $element->getCollection();
        $collection
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('all_children')
            ->setOrder('position', Varien_Db_Select::SQL_ASC);

        return $collection;
    }

    /**
     * @param $element
     *
     * @return mixed
     */
    public function getChildrenElements($element)
    {
        $collection = $this->_getChildrenElementsBase($element);
        $collection->addAttributeToFilter('is_active', 1)
            ->addIdFilter($element->getChildren())
            ->load();

        return $collection;
    }

    /**
     * @param $element
     *
     * @return mixed
     */
    public function getChildrenElementsWithInactive($element)
    {
        $collection = $this->_getChildrenElementsBase($element);
        $collection->addFieldToFilter('parent_id', $element->getId());

        return $collection;
    }

    /**
     * @param      $element
     * @param bool $recursive
     *
     * @return Varien_Db_Select
     */
    protected function _getChildrenIdSelect($element, $recursive = true)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $adapter->select()
            ->from(array('m' => $this->getEntityTable()), 'entity_id')
            ->where($adapter->quoteIdentifier('path') . ' LIKE ?', $element->getPath() . '/%');

        if (!$recursive) {
            $select->where($adapter->quoteIdentifier('level') . ' <= ?', $element->getLevel() + 1);
        }
        return $select;
    }

    /**
     * @param      $element
     * @param bool $recursive
     *
     * @return array
     */
    public function getChildren($element, $recursive = true)
    {
        $attributeId  = (int)$this->_getIsActiveAttributeId();
        $backendTable = $this->getTable(array($this->getEntityTablePrefix(), 'int'));
        $adapter      = $this->_getReadAdapter();
        $checkSql     = $adapter->getCheckSql('c.value_id > 0', 'c.value', 'd.value');
        $bind         = array(
            'attribute_id' => $attributeId,
            'store_id'     => $element->getStoreId(),
            'scope'        => 1,
        );
        $select       = $this->_getChildrenIdSelect($element, $recursive);
        $select
            ->joinLeft(
                array('d' => $backendTable),
                'd.attribute_id = :attribute_id AND d.store_id = 0 AND d.entity_id = m.entity_id',
                array()
            )
            ->joinLeft(
                array('c' => $backendTable),
                'c.attribute_id = :attribute_id AND c.store_id = :store_id AND c.entity_id = m.entity_id',
                array()
            )
            ->where($checkSql . ' = :scope');

        return $adapter->fetchCol($select, $bind);
    }

    /**
     * @param      $element
     * @param bool $recursive
     *
     * @return array
     */
    public function getChildrenIds($element, $recursive = true)
    {
        $select = $this->_getChildrenIdSelect($element, $recursive);
        return $this->_getReadAdapter()->fetchCol($select);
    }

    /**
     * @param $element
     *
     * @return array
     */
    public function getAllChildren($element)
    {
        $children = $this->getChildren($element);
        $myId     = array($element->getId());
        $children = array_merge($myId, $children);

        return $children;
    }

    /**
     * @param $element
     *
     * @return bool
     */
    public function isInRootElementList($element)
    {
        $rootElementId = 0;

        if (Mage::app()->getStore()->getGroup()) {
            $rootElementId = Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID);
        }

        return in_array($rootElementId, $element->getParentIds());
    }

    /**
     * @param $elementId
     *
     * @return bool
     */
    public function isForbiddenToDelete($elementId)
    {
        $rootElementIds   = array();
        $rootElementIds[] = Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID;

        foreach (Mage::app()->getStores() as $store) {
            $rootElementIds[] = Mage::getStoreConfig(Poebel_CmsNavigation_Helper_Element::XML_PATH_ELEMENT_ROOT_ID, $store->getId());
        }

        return in_array($elementId, $rootElementIds);
    }

    /**
     * @param $elementId
     *
     * @return string
     */
    public function getCategoryPathById($elementId)
    {
        $select = $this->getReadConnection()->select()
            ->from($this->getEntityTable(), array('path'))
            ->where('entity_id = :entity_id');
        $bind   = array('entity_id' => (int)$elementId);

        return $this->getReadConnection()->fetchOne($select, $bind);
    }

    /**
     * @param Poebel_CmsNavigation_Model_Element $element
     * @param Poebel_CmsNavigation_Model_Element $newParent
     * @param null                               $afterElementId
     *
     * @return $this
     */
    public function changeParent(Poebel_CmsNavigation_Model_Element $element, Poebel_CmsNavigation_Model_Element $newParent, $afterElementId = null)
    {
        $childrenCount = $this->getChildrenCount($element->getId()) + 1;
        $table         = $this->getEntityTable();
        $adapter       = $this->_getWriteAdapter();
        $levelFiled    = $adapter->quoteIdentifier('level');
        $pathField     = $adapter->quoteIdentifier('path');

        $adapter->update(
            $table,
            array('children_count' => new Zend_Db_Expr('children_count - ' . $childrenCount)),
            array('entity_id IN(?)' => $element->getParentIds())
        );

        $adapter->update(
            $table,
            array('children_count' => new Zend_Db_Expr('children_count + ' . $childrenCount)),
            array('entity_id IN(?)' => $newParent->getPathIds())
        );

        $position = $this->_processPositions($element, $newParent, $afterElementId);

        $newPath          = sprintf('%s/%s', $newParent->getPath(), $element->getId());
        $newLevel         = $newParent->getLevel() + 1;
        $levelDisposition = $newLevel - $element->getLevel();

        $adapter->update(
            $table,
            array(
                'path'  => new Zend_Db_Expr('REPLACE(' . $pathField . ',' .
                    $adapter->quote($element->getPath() . '/') . ', ' . $adapter->quote($newPath . '/') . ')'
                ),
                'level' => new Zend_Db_Expr($levelFiled . ' + ' . $levelDisposition)
            ),
            array($pathField . ' LIKE ?' => $element->getPath() . '/%')
        );

        $data = array(
            'path'      => $newPath,
            'level'     => $newLevel,
            'position'  => $position,
            'parent_id' => $newParent->getId()
        );
        $adapter->update($table, $data, array('entity_id = ?' => $element->getId()));

        $element->addData($data);

        return $this;
    }

    /**
     * @param $element
     * @param $newParent
     * @param $afterElementId
     *
     * @return int|string
     */
    protected function _processPositions($element, $newParent, $afterElementId)
    {
        $table         = $this->getEntityTable();
        $adapter       = $this->_getWriteAdapter();
        $positionField = $adapter->quoteIdentifier('position');

        $bind  = array(
            'position' => new Zend_Db_Expr($positionField . ' - 1')
        );
        $where = array(
            'parent_id = ?'         => $element->getParentId(),
            $positionField . ' > ?' => $element->getPosition()
        );
        $adapter->update($table, $bind, $where);

        if ($afterElementId) {
            $select   = $adapter->select()
                ->from($table, 'position')
                ->where('entity_id = :entity_id');
            $position = $adapter->fetchOne($select, array('entity_id' => $afterElementId));

            $bind  = array(
                'position' => new Zend_Db_Expr($positionField . ' + 1')
            );
            $where = array(
                'parent_id = ?'         => $newParent->getId(),
                $positionField . ' > ?' => $position
            );
            $adapter->update($table, $bind, $where);
        } elseif ($afterElementId !== null) {
            $position = 0;
            $bind     = array(
                'position' => new Zend_Db_Expr($positionField . ' + 1')
            );
            $where    = array(
                'parent_id = ?'         => $newParent->getId(),
                $positionField . ' > ?' => $position
            );
            $adapter->update($table, $bind, $where);
        } else {
            $select   = $adapter->select()
                ->from($table, array('position' => new Zend_Db_Expr('MIN(' . $positionField . ')')))
                ->where('parent_id = :parent_id');
            $position = $adapter->fetchOne($select, array('parent_id' => $newParent->getId()));
        }
        $position += 1;

        return $position;
    }
}