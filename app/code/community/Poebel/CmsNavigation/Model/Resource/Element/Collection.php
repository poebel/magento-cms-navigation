<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Model_Resource_Element_Collection extends Poebel_CmsNavigation_Model_Resource_Collection_Abstract
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'poebel_cmsnavigation_element_collection';
    /**
     * @var string
     */
    protected $_eventObject = 'element_collection';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('poebel_cmsnavigation/element');
    }

    /**
     * @param $elementIds
     *
     * @return $this
     */
    public function addIdFilter($elementIds)
    {
        $condition = '';
        if (is_array($elementIds)) {
            if (empty($elementIds)) {
                $condition = '';
            } else {
                $condition = array('in' => $elementIds);
            }
        } elseif (is_numeric($elementIds)) {
            $condition = $elementIds;
        } elseif (is_string($elementIds)) {
            $ids = explode(',', $elementIds);
            if (empty($ids)) {
                $condition = $elementIds;
            } else {
                $condition = array('in' => $ids);
            }
        }
        $this->addFieldToFilter('entity_id', $condition);
        return $this;
    }

    /**
     * @return Varien_Data_Collection_Db
     */
    protected function _beforeLoad()
    {
        Mage::dispatchEvent(
            $this->_eventPrefix . '_load_before',
            array($this->_eventObject => $this)
        );
        return parent::_beforeLoad();
    }

    /**
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _afterLoad()
    {
        Mage::dispatchEvent(
            $this->_eventPrefix . '_load_after',
            array($this->_eventObject => $this)
        );

        return parent::_afterLoad();
    }

    /**
     * @param $regexp
     *
     * @return $this
     */
    public function addPathFilter($regexp)
    {
        $this->addFieldToFilter('path', array('regexp' => $regexp));
        return $this;
    }

    /**
     * @return int
     */
    protected function _getCurrentStoreId()
    {
        return (int)Mage::app()->getStore()->getId();
    }

    /**
     * @return $this
     */
    public function addIsActiveFilter()
    {
        $this->addAttributeToFilter('is_active', 1);
        Mage::dispatchEvent(
            $this->_eventPrefix . '_add_is_active_filter',
            array($this->_eventObject => $this)
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function addNameToResult()
    {
        $this->addAttributeToSelect('name');
        return $this;
    }

    /**
     * @param $paths
     *
     * @return $this
     */
    public function addPathsFilter($paths)
    {
        if (!is_array($paths)) {
            $paths = array($paths);
        }
        $write = $this->getResource()->getWriteConnection();
        $cond  = array();
        foreach ($paths as $path) {
            $cond[] = $write->quoteInto('e.path LIKE ?', "$path%");
        }
        if ($cond) {
            $this->getSelect()->where(join(' OR ', $cond));
        }
        return $this;
    }

    /**
     * @param $level
     *
     * @return $this
     */
    public function addLevelFilter($level)
    {
        $this->addFieldToFilter('level', array('lteq' => $level));
        return $this;
    }

    /**
     * @return $this
     */
    public function addRootLevelFilter()
    {
        $this->addFieldToFilter('path', array('neq' => '1'));
        $this->addLevelFilter(1);
        return $this;
    }

    /**
     * @param $field
     *
     * @return $this
     */
    public function addOrderField($field)
    {
        $this->setOrder($field, self::SORT_ORDER_ASC);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewEmptyItem()
    {
        return new $this->_itemObjectClass();
    }
}
