<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Model_Resource_Element_Tree extends Varien_Data_Tree_Dbp
{
    const ID_FIELD    = 'id';
    const PATH_FIELD  = 'path';
    const ORDER_FIELD = 'order';
    const LEVEL_FIELD = 'level';

    /**
     * @var null
     */
    protected $_collection = null;
    /**
     * @var null
     */
    protected $_isActiveAttributeId = null;
    /**
     * @var null
     */
    protected $_inactiveItems = null;
    /**
     * @var null
     */
    protected $_inactiveElementIds = null;
    /**
     * @var null
     */
    protected $_storeId = null;

    /**
     *
     */
    public function __construct()
    {
        $resource = Mage::getSingleton('core/resource');

        parent::__construct(
            $resource->getConnection('poebel_cmsnavigation_write'),
            $resource->getTableName('poebel_cmsnavigation/element'),
            array(
                Varien_Data_Tree_Dbp::ID_FIELD    => 'entity_id',
                Varien_Data_Tree_Dbp::PATH_FIELD  => 'path',
                Varien_Data_Tree_Dbp::ORDER_FIELD => 'position',
                Varien_Data_Tree_Dbp::LEVEL_FIELD => 'level',
            )
        );
    }

    /**
     * @param $storeId
     *
     * @return $this
     */
    public function setStoreId($storeId)
    {
        $this->_storeId = (int)$storeId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStoreId()
    {
        if ($this->_storeId === null) {
            $this->_storeId = Mage::app()->getStore()->getId();
        }
        return $this->_storeId;
    }

    /**
     * @param null  $collection
     * @param bool  $sorted
     * @param array $exclude
     * @param bool  $toLoad
     * @param bool  $onlyActive
     *
     * @return $this
     */
    public function addCollectionData($collection = null, $sorted = false, $exclude = array(), $toLoad = true, $onlyActive = false)
    {
        if (is_null($collection)) {
            $collection = $this->getCollection($sorted);
        } else {
            $this->setCollection($collection);
        }

        if (!is_array($exclude)) {
            $exclude = array($exclude);
        }

        $nodeIds = array();
        foreach ($this->getNodes() as $node) {
            if (!in_array($node->getId(), $exclude)) {
                $nodeIds[] = $node->getId();
            }
        }
        $collection->addIdFilter($nodeIds);
        if ($onlyActive) {
            $disabledIds = $this->_getDisabledIds($collection);
            if ($disabledIds) {
                $collection->addFieldToFilter('entity_id', array('nin' => $disabledIds));
            }
            $collection->addAttributeToFilter('is_active', 1);
        }

        if ($toLoad) {
            $collection->load();

            foreach ($collection as $element) {
                if ($this->getNodeById($element->getId())) {
                    $this->getNodeById($element->getId())
                        ->addData($element->getData());
                }
            }

            foreach ($this->getNodes() as $node) {
                if (!$collection->getItemById($node->getId()) && $node->getParent()) {
                    $this->removeNode($node);
                }
            }
        }

        return $this;
    }

    /**
     * @param $ids
     *
     * @return $this
     */
    public function addInactiveElementIds($ids)
    {
        if (!is_array($this->_inactiveElementIds)) {
            $this->_initInactiveElementIds();
        }
        $this->_inactiveElementIds = array_merge($ids, $this->_inactiveElementIds);
        return $this;
    }

    /**
     * @return $this
     */
    protected function _initInactiveElementIds()
    {
        $this->_inactiveElementIds = array();
        Mage::dispatchEvent('poebel_cmsnavigatiob_element_tree_init_inactive_element_ids', array('tree' => $this));
        return $this;
    }

    /**
     * @return array|null
     */
    public function getInactiveElementIds()
    {
        if (!is_array($this->_inactiveElementIds)) {
            $this->_initInactiveElementIds();
        }

        return $this->_inactiveElementIds;
    }

    /**
     * @param $collection
     *
     * @return array
     */
    protected function _getDisabledIds($collection)
    {
        $storeId = Mage::app()->getStore()->getId();

        $this->_inactiveItems = $this->getInactiveElementIds();

        $this->_inactiveItems = array_merge(
            $this->_getInactiveItemIds($collection, $storeId),
            $this->_inactiveItems
        );

        $allIds      = $collection->getAllIds();
        $disabledIds = array();

        foreach ($allIds as $id) {
            $parents = $this->getNodeById($id)->getPath();
            foreach ($parents as $parent) {
                if (!$this->_getItemIsActive($parent->getId(), $storeId)) {
                    $disabledIds[] = $id;
                    continue;
                }
            }
        }
        return $disabledIds;
    }

    /**
     * @return null|string
     */
    protected function _getIsActiveAttributeId()
    {
        $resource = Mage::getSingleton('core/resource');
        if (is_null($this->_isActiveAttributeId)) {
            $bind   = array(
                'entity_type_code' => Poebel_CmsNavigation_Model_Element::ENTITY,
                'attribute_code'   => 'is_active'
            );
            $select = $this->_conn->select()
                ->from(array('a' => $resource->getTableName('eav/attribute')), array('attribute_id'))
                ->join(array('t' => $resource->getTableName('eav/entity_type')), 'a.entity_type_id = t.entity_type_id')
                ->where('entity_type_code = :entity_type_code')
                ->where('attribute_code = :attribute_code');

            $this->_isActiveAttributeId = $this->_conn->fetchOne($select, $bind);
        }
        return $this->_isActiveAttributeId;
    }

    /**
     * @param $collection
     * @param $storeId
     *
     * @return array
     */
    protected function _getInactiveItemIds($collection, $storeId)
    {
        $filter      = $collection->getAllIdsSql();
        $attributeId = $this->_getIsActiveAttributeId();

        $conditionSql = $this->_conn->getCheckSql('c.value_id > 0', 'c.value', 'd.value');
        $table        = Mage::getSingleton('core/resource')->getTableName(array('poebel_cmsnavigation/element', 'int'));
        $bind         = array(
            'attribute_id'  => $attributeId,
            'store_id'      => $storeId,
            'zero_store_id' => 0,
            'cond'          => 0,

        );
        $select       = $this->_conn->select()
            ->from(array('d' => $table), array('d.entity_id'))
            ->where('d.attribute_id = :attribute_id')
            ->where('d.store_id = :zero_store_id')
            ->where('d.entity_id IN (?)', new Zend_Db_Expr($filter))
            ->joinLeft(
                array('c' => $table),
                'c.attribute_id = :attribute_id AND c.store_id = :store_id AND c.entity_id = d.entity_id',
                array()
            )
            ->where($conditionSql . ' = :cond');

        return $this->_conn->fetchCol($select, $bind);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    protected function _getItemIsActive($id)
    {
        if (!in_array($id, $this->_inactiveItems)) {
            return true;
        }
        return false;
    }

    /**
     * @param bool $sorted
     *
     * @return null|Poebel_CmsNavigation_Model_Resource_Element_Collection
     */
    public function getCollection($sorted = false)
    {
        if (is_null($this->_collection)) {
            $this->_collection = $this->_getDefaultCollection($sorted);
        }
        return $this->_collection;
    }

    /**
     * @param $collection
     *
     * @return $this
     */
    public function setCollection($collection)
    {
        if (!is_null($this->_collection)) {
            destruct($this->_collection);
        }
        $this->_collection = $collection;
        return $this;
    }

    /**
     * @param bool $sorted
     *
     * @return Poebel_CmsNavigation_Model_Resource_Element_Collection
     */
    protected function _getDefaultCollection($sorted = false)
    {
        /** @var Poebel_CmsNavigation_Model_Resource_Element_Collection $collection */
        $collection = Mage::getResourceModel('poebel_cmsnavigation/element_collection');

        $attributes = Mage::getConfig()->getNode('frontend/poebel_cmsnavigation/element/collection/attributes');
        if ($attributes) {
            $attributes = $attributes->asArray();
            $attributes = array_keys($attributes);
        }
        $collection->addAttributeToSelect($attributes);

        if ($sorted) {
            if (is_string($sorted)) {
                // $sorted is supposed to be attribute name
                $collection->addAttributeToSort($sorted);
            } else {
                $collection->addAttributeToSort('name');
            }
        }

        return $collection;
    }

    /**
     * @param $element
     * @param $newParent
     * @param $prevNode
     *
     * @return $this
     */
    protected function _beforeMove($element, $newParent, $prevNode)
    {
        Mage::dispatchEvent(
            'poebel_cmsnavigation_element_tree_move_before', array(
                'element'     => $element,
                'prev_parent' => $prevNode,
                'parent'      => $newParent
            )
        );

        return $this;
    }

    /**
     * @param Varien_Data_Tree_Node $element
     * @param Varien_Data_Tree_Node $newParent
     * @param null                  $prevNode
     */
    public function move($element, $newParent, $prevNode = null)
    {
        $this->_beforeMove($element, $newParent, $prevNode);
        Mage::getResourceSingleton('poebel_cmsnavigation/element')->move($element->getId(), $newParent->getId());
        parent::move($element, $newParent, $prevNode);

        $this->_afterMove($element, $newParent, $prevNode);
    }

    /**
     * @param $element
     * @param $newParent
     * @param $prevNode
     *
     * @return $this
     */
    protected function _afterMove($element, $newParent, $prevNode)
    {
        Mage::app()->cleanCache(array(Poebel_CmsNavigation_Model_Element::CACHE_TAG));

        Mage::dispatchEvent(
            'poebel_cmsnavigation_element_tree_move_after', array(
                'element'   => $element,
                'prev_node' => $prevNode,
                'parent'    => $newParent
            )
        );

        return $this;
    }

    /**
     * @param      $ids
     * @param bool $addCollectionData
     *
     * @return $this|bool
     */
    public function loadByIds($ids, $addCollectionData = true)
    {
        $levelField = $this->_conn->quoteIdentifier('level');
        $pathField  = $this->_conn->quoteIdentifier('path');
        // load first two levels, if no ids specified
        if (empty($ids)) {
            $select = $this->_conn->select()
                ->from($this->_table, 'entity_id')
                ->where($levelField . ' <= 2');
            $ids    = $this->_conn->fetchCol($select);
        }
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        foreach ($ids as $key => $id) {
            $ids[$key] = (int)$id;
        }

        $select = $this->_conn->select()
            ->from($this->_table, array('path', 'level'))
            ->where('entity_id IN (?)', $ids);
        $where  = array($levelField . '=0' => true);

        foreach ($this->_conn->fetchAll($select) as $item) {
            $pathIds = explode('/', $item['path']);
            $level   = (int)$item['level'];
            while ($level > 0) {
                $pathIds[count($pathIds) - 1]                            = '%';
                $path                                                    = implode('/', $pathIds);
                $where["$levelField=$level AND $pathField LIKE '$path'"] = true;
                array_pop($pathIds);
                $level--;
            }
        }
        $where = array_keys($where);

        if ($addCollectionData) {
            $select = $this->_createCollectionDataSelect();
        } else {
            $select = clone $this->_select;
            $select->order($this->_orderField . ' ' . Varien_Db_Select::SQL_ASC);
        }
        $select->where(implode(' OR ', $where));

        $arrNodes = $this->_conn->fetchAll($select);
        if (!$arrNodes) {
            return false;
        }

        $childrenItems = array();
        foreach ($arrNodes as $key => $nodeInfo) {
            $pathToParent = explode('/', $nodeInfo[$this->_pathField]);
            array_pop($pathToParent);
            $pathToParent                   = implode('/', $pathToParent);
            $childrenItems[$pathToParent][] = $nodeInfo;
        }
        $this->addChildNodes($childrenItems, '', null);
        return $this;
    }

    /**
     * @param      $path
     * @param bool $addCollectionData
     * @param bool $withRootNode
     *
     * @return array
     */
    public function loadBreadcrumbsArray($path, $addCollectionData = true, $withRootNode = false)
    {
        $pathIds = explode('/', $path);
        if (!$withRootNode) {
            array_shift($pathIds);
        }
        $result = array();
        if (!empty($pathIds)) {
            if ($addCollectionData) {
                $select = $this->_createCollectionDataSelect(false);
            } else {
                $select = clone $this->_select;
            }
            $select
                ->where('e.entity_id IN(?)', $pathIds)
                ->order($this->_conn->getLengthSql('e.path') . ' ' . Varien_Db_Select::SQL_ASC);
            $result = $this->_conn->fetchAll($select);
        }
        return $result;
    }

    /**
     * @param bool  $sorted
     * @param array $optionalAttributes
     *
     * @return Varien_Db_Select
     */
    protected function _createCollectionDataSelect($sorted = true, $optionalAttributes = array())
    {
        $select = $this->_getDefaultCollection($sorted ? $this->_orderField : false)
            ->getSelect();

        $attributes = array('name', 'is_active');
        if ($optionalAttributes) {
            $attributes = array_unique(array_merge($attributes, $optionalAttributes));
        }

        foreach ($attributes as $attributeCode) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute */
            $attribute = Mage::getResourceSingleton('poebel_cmsnavigation/element')->getAttribute($attributeCode);

            if (!$attribute->getBackend()->isStatic()) {
                $tableDefault = sprintf('d_%s', $attributeCode);
                $tableStore   = sprintf('s_%s', $attributeCode);
                $valueExpr    = $this->_conn
                    ->getCheckSql("{$tableStore}.value_id > 0", "{$tableStore}.value", "{$tableDefault}.value");

                $select
                    ->joinLeft(
                        array($tableDefault => $attribute->getBackend()->getTable()),
                        sprintf(
                            '%1$s.entity_id=e.entity_id AND %1$s.attribute_id=%2$d'
                            . ' AND %1$s.entity_type_id=e.entity_type_id AND %1$s.store_id=%3$d',
                            $tableDefault, $attribute->getId(), Mage_Core_Model_App::ADMIN_STORE_ID
                        ),
                        array($attributeCode => 'value')
                    )
                    ->joinLeft(
                        array($tableStore => $attribute->getBackend()->getTable()),
                        sprintf(
                            '%1$s.entity_id=e.entity_id AND %1$s.attribute_id=%2$d'
                            . ' AND %1$s.entity_type_id=e.entity_type_id AND %1$s.store_id=%3$d',
                            $tableStore, $attribute->getId(), $this->getStoreId()
                        ),
                        array($attributeCode => $valueExpr)
                    );
            }
        }

        return $select;
    }

    /**
     * @param $ids
     *
     * @return array
     */
    public function getExistingElementIdsBySpecifiedIds($ids)
    {
        if (empty($ids)) {
            return array();
        }
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        $select = $this->_conn->select()
            ->from($this->_table, array('entity_id'))
            ->where('entity_id IN (?)', $ids);
        return $this->_conn->fetchCol($select);
    }
}