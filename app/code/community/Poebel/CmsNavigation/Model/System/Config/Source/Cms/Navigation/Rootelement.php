<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Model_System_Config_Source_Cms_Navigation_Rootelement
{
    /**
     * @var null
     */
    protected $_options = null;

    /**
     *
     */
    protected function _initOptions()
    {
        if (!$this->_options) {
            $this->_options = array();

            $collection = Mage::getResourceModel('poebel_cmsnavigation/element_collection')
                ->addAttributeToFilter('entity_id', array('neq' => Poebel_CmsNavigation_Model_Element::TREE_ROOT_ID))
                ->addAttributeToFilter('level', 1)
                ->addAttributeToSelect('name');

            foreach ($collection as $element) {
                $this->_options[] = array(
                    'value' => $element->getId(),
                    'label' => $element->getName(),
                );
            }
        }
    }

    /**
     * @return null
     */
    public function getAllOptions()
    {
        $this->_initOptions();
        return $this->_options;
    }

    /**
     * @return null
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
