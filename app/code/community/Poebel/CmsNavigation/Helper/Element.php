<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */

class Poebel_CmsNavigation_Helper_Element extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ELEMENT_ROOT_ID  = 'cms/navigation/root_element';
    const XML_PATH_PARENT_IS_ACTIVE = 'cms/navigation/parent_is_active';
    const XML_PATH_MAX_RENDER_DEPTH = 'cms/navigation/max_render_depth';

    /**
     * @var array
     */
    protected $_storeElements = array();

    /**
     * @param bool $sorted
     * @param bool $asCollection
     * @param bool $toLoad
     *
     * @return array|Varien_Data_Collection
     */
    public function getStoreElements($sorted = false, $asCollection = false, $toLoad = true)
    {
        $parent = 0;
        if (Mage::app()->getStore()->getGroup()) {
            $parent = Mage::getStoreConfig(self::XML_PATH_ELEMENT_ROOT_ID);
        }

        $cacheKey = sprintf('%d-%d-%d-%d', $parent, $sorted, $asCollection, $toLoad);
        if (isset($this->_storeElements[$cacheKey])) {
            return $this->_storeElements[$cacheKey];
        }

        $element = Mage::getModel('poebel_cmsnavigation/element');
        if (!$element->checkId($parent)) {
            if ($asCollection) {
                return new Varien_Data_Collection();
            }
            return array();
        }

        $recursionLevel = max(0, (int)Mage::getStoreConfig(self::XML_PATH_MAX_RENDER_DEPTH));
        $storeElements  = $element->getElements($parent, $recursionLevel, $sorted, $asCollection, $toLoad);

        $this->_storeElements[$cacheKey] = $storeElements;

        return $storeElements;
    }

    /**
     * @param $element
     *
     * @return bool
     */
    public function isActiveMenuElement($element)
    {
        $isActive = false;

        switch ($this->getElementAttributeRawValue($element, 'mode')) {
        case Poebel_CmsNavigation_Model_Element::MODE_ACTION:
            $action = Mage::app()->getRequest()->getRouteName() . '/' . Mage::app()->getRequest()->getControllerName() . '/' . Mage::app()->getRequest()->getActionName();
            if (strpos($this->getElementAttributeRawValue($element, 'action'), $action) !== false) {
                $isActive = true;
            }
        case Poebel_CmsNavigation_Model_Element::MODE_URL:
            // No support for arbitrary urls, this mode is intended for external urls
            break;
        case Poebel_CmsNavigation_Model_Element::MODE_PRODUCT:
            if (Mage::registry('current_product') && Mage::registry('current_product')->getId() == $this->getElementAttributeRawValue($element, 'product_id')) {
                $isActive = true;
            }
            break;
        case Poebel_CmsNavigation_Model_Element::MODE_CATEGORY:
            if (Mage::registry('current_category') && Mage::registry('current_category')->getId() == $this->getElementAttributeRawValue($element, 'category_id')) {
                $isActive = true;
            }
            break;
        case Poebel_CmsNavigation_Model_Element::MODE_CMS_PAGE:
            if (Mage::getSingleton('cms/page')->getIdentifier() == $this->getElementAttributeRawValue($element, 'cms_page_id')) {
                $isActive = true;
            }
            break;
        }

        return $isActive;
    }

    /**
     * @param $element
     *
     * @return string
     */
    public function getElementUrl($element)
    {
        $url = '#';

        switch ($this->getElementAttributeRawValue($element, 'mode')) {
        case Poebel_CmsNavigation_Model_Element::MODE_ACTION:
            $url = Mage::getUrl(
                $this->getElementAttributeRawValue($element, 'action'), array(
                    '_secure' => Mage::app()->getStore()->isCurrentlySecure(),
                )
            );
            break;
        case Poebel_CmsNavigation_Model_Element::MODE_URL:
            $url = $this->getElementAttributeRawValue($element, 'url');
            break;
        case Poebel_CmsNavigation_Model_Element::MODE_PRODUCT:
            if (Mage::helper('core')->isModuleEnabled('Mage_Catalog')) {
                $url = Mage::helper('catalog/product')->getProductUrl($this->getElementAttributeRawValue($element, 'product_id'));
            }
            break;
        case Poebel_CmsNavigation_Model_Element::MODE_CATEGORY:
            if (Mage::helper('core')->isModuleEnabled('Mage_Catalog')) {
                $url = Mage::helper('catalog/category')->getCategoryUrl($this->getElementAttributeRawValue($element, 'category_id'));
            }
            break;
        case Poebel_CmsNavigation_Model_Element::MODE_CMS_PAGE:
            $_cmsPageId = $this->getElementAttributeRawValue($element, 'cms_page_id');
            if ($_cmsPageId) {
                if ($_cmsPageId == Mage::getStoreConfig('web/default/cms_home_page')) {
                    $url = Mage::getUrl('/');
                } else {
                    $url = Mage::helper('cms/page')->getPageUrl($_cmsPageId);
                }
            }
            break;
        }

        return $url;
    }

    /**
     * @param      $elementId
     * @param      $attributeCode
     * @param null $storeId
     *
     * @return mixed
     */
    public function getElementAttributeRawValue($elementId, $attributeCode, $storeId = null)
    {
        if ($elementId instanceof Varien_Object) {
            $elementId = $elementId->getId();
        }

        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }

        return Mage::getResourceSingleton('poebel_cmsnavigation/element')->getAttributeRawValue($elementId, $attributeCode, $storeId);
    }
}