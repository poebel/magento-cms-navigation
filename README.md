# CMS Navigation for Magento
This module adds the possibility to create and manage an hierarchical navigation for CMS pages, actions or arbitrary urls.  
The navigation is rendered like the normal top category navigation, currently all new elements are rendered after the categories.

### Installation
- Install module
- Clear cache, logout and login if you were already logged in to the backend
- Go to "CMS -> Navigation" and start managing your navigation elements (the interface is almost identical to the category management)
- Advanced configuration options could be found under "System -> Configuration -> Content Management -> Navigation"

### Current limitations
- Linking to categories or products is not yet implemented

### Compatibility
Tested with:  
- Magento CE 1.8.0.1  
- [Magento Lite 1.8.0.1](https://github.com/colinmollenhour/magento-lite)

### Additional information
This module is based on the category part of the standard Mage_Catalog module but designed to work completely independent from it.

### Current Version
0.2.1.0

### License
[OSL-3.0](http://opensource.org/licenses/OSL-3.0)